<?php

namespace App\Services\V1\Vendor\Gcp;

use Glamstack\GoogleCloud\ApiClient;
use Illuminate\Support\Str;

class CloudIamServiceAccountKeyService
{
    use BaseService;

    public const API_SCOPES = [
        'https://www.googleapis.com/auth/iam',
        'https://www.googleapis.com/auth/cloud-platform'
    ];

    /* GCP Project ID */
    private string $project_id;

    /* GCP JSON API Key File Path */
    private string $json_key_file_path;

    /* glamstack/google-cloud-sdk */
    private ApiClient $api_client;

    /**
     * Initialize the Service
     *
     * @param string $cloud_provider_id
     *      The UUID of the CloudProvider record
     *
     * @param string $project_id
     *      The Google Project ID of the IAM permissions to update
     *
     * @note Once we have the IAM endpoints setup in the SDK we can remove the
     * project_id input variable
     */
    public function __construct($cloud_provider_id, string $project_id)
    {
        // Call BaseService methods to establish API connection
        $this->setCloudProvider($cloud_provider_id);
        $this->setProjectId($project_id);
        $this->setJsonKeyFilePath();
        $this->api_client = $this->setGlamstackGoogleCloudSdk(self::API_SCOPES, $this->json_key_file_path, $this->project_id);
    }

    /**
     * Set the `json_key_file_path` parameter
     *
     * @return void
     */
    protected function setJsonKeyFilePath(): void
    {
        $this->json_key_file_path = storage_path('keys/cloud_providers/' . $this->cloud_provider->id . '.json');
    }

    /**
     * Get the value of `json_key_file_path` parameter
     *
     * @return string
     */
    public function getJsonKeyFilePath(): string
    {
        return $this->json_key_file_path;
    }

    /**
     * Set the GCP Project ID class variable
     *
     * @see https://cloud.google.com/resource-manager/docs/creating-managing-projects
     *
     * @param string $project_id
     *      ```
     *      (example) 123456789012
     *      (example) my-project-a1b2c3
     *      ```
     *
     * @return void
     */
    public function setProjectId(string $project_id): void
    {
        // Check if project has prefix of `projects/` and remove if exists
        if (Str::startsWith($project_id, 'projects/')) {
            $this->project_id = str_replace('projects/', '', $project_id);
        } else {
            $this->project_id = $project_id;
        }
    }

    /**
     * Get the GCP Project ID class variable value
     *
     * @see https://cloud.google.com/resource-manager/docs/creating-managing-projects
     *
     * @return string
     *      ```
     *      (example) 123456789012
     *      (example) my-project-a1b2c3
     *      ```
     */
    public function getProjectId()
    {
        return $this->project_id;
    }

    /**
     * Get list of keys for a GCP Service Account
     *
     * @see https://cloud.google.com/iam/docs/reference/rest/v1/projects.serviceAccounts.keys/list
     * @uses iam.serviceAccountKeys.list
     *
     * @param string $iam_email
     *      User or service account email address
     *      ```
     *      (example) pcook@example.com
     *      (example) hackystack-provisioner@my-project.iam.gserviceaccount.com
     *      ```
     *
     * @return object
     */
    public function list($iam_email)
    {
        $uri = 'https://iam.googleapis.com/v1/projects/' . $this->project_id . '/serviceAccounts/' . $iam_email . '/keys';
        $response = $this->api_client->rest()->get($uri, []);
        return $response;
    }

    /**
     * Get a specific key for a GCP Service Account
     *
     * @see https://cloud.google.com/iam/docs/reference/rest/v1/projects.serviceAccounts.keys/get
     * @uses iam.serviceAccountKeys.get
     *
     * @param string $iam_email
     *      User or service account email address
     *      ```
     *      (example) hackystack-provisioner@my-project.iam.gserviceaccount.com
     *      ```
     *
     * @param string $key
     *      Service account key ID or name
     *      ```
     *      (example) projects/jmartin-92932d23/serviceAccounts/hackystack-unit-test-3625@jmartin-92932d23.iam.gserviceaccount.com/keys/2e063ae7bdefe06ddef4a6ead15aaac6a9cb6320
     *      (example) 2e063ae7bdefe06ddef4a6ead15aaac6a9cb6320
     *      ```
     *
     * @param bool $trim (default: false)
     *      Whether a full key name is provided that needs to be trimmed
     *
     * @return object
     */
    public function get($iam_email, $key_id, $trim = false)
    {
        if ($trim == true) {
            $parsed_key = substr($key_id, strrpos($key_id, '/')+1);
        } else {
            $parsed_key = $key_id;
        }

        $uri = 'https://iam.googleapis.com/v1/projects/' . $this->project_id . '/serviceAccounts/' . $iam_email . '/keys/' . $parsed_key;
        $response = $this->api_client->rest()->get($uri, []);
        return $response;
    }

    /**
     * Create a new key for a GCP Service Account Key
     *
     * @see https://cloud.google.com/iam/docs/reference/rest/v1/projects.serviceAccounts.keys/create
     * @uses iam.serviceAccountKeys.create
     *
     * @param string $iam_email
     *      Service account email address
     *      ```
     *      (example) hackystack-provisioner@my-project.iam.gserviceaccount.com
     *      ```
     *
     * @return object
     *      ```json
     *      {
     *        +"headers": {
     *          +"Content-Type": "application/json; charset=UTF-8"
     *          +"Vary": "X-Origin Referer Origin,Accept-Encoding"
     *          +"Date": "Tue, 24 May 2022 00:51:19 GMT"
     *          +"Server": "ESF"
     *          +"Cache-Control": "private"
     *          +"X-XSS-Protection": "0"
     *          +"X-Frame-Options": "SAMEORIGIN"
     *          +"X-Content-Type-Options": "nosniff"
     *          +"Alt-Svc": "h3=":443"; ma=2592000,h3-29=":443"; ma=2592000,h3-Q050=":443"; ma=2592000,h3-Q046=":443"; ma=2592000,h3-Q043=":443"; ma=2592000,quic=":443"; ma=2592000; v="46,43""
     *          +"Accept-Ranges": "none"
     *          +"Transfer-Encoding": "chunked"
     *        }
     *        +"json": "{"name":"projects\/jmartin-92932d23\/serviceAccounts\/hackystack-unit-test-3625@jmartin-92932d23.iam.gserviceaccount.com\/keys\/2e063ae7bdefe06ddef4a6ead15aaac6a9cb6320","privateKeyType":"TYPE_GOOGLE_CREDENTIALS_FILE","privateKeyData":"(base64)","validAfterTime":"2022-05-24T00:51:19Z","validBeforeTime":"9999-12-31T23:59:59Z","keyAlgorithm":"KEY_ALG_RSA_2048","keyOrigin":"GOOGLE_PROVIDED","keyType":"USER_MANAGED"}"
     *        +"object": {
     *          +"name": "projects/jmartin-92932d23/serviceAccounts/hackystack-unit-test-3625@jmartin-92932d23.iam.gserviceaccount.com/keys/2e063ae7bdefe06ddef4a6ead15aaac6a9cb6320"
     *          +"privateKeyType": "TYPE_GOOGLE_CREDENTIALS_FILE"
     *          +"privateKeyData": "(base64)"
     *          +"validAfterTime": "2022-05-24T00:51:19Z"
     *          +"validBeforeTime": "9999-12-31T23:59:59Z"
     *          +"keyAlgorithm": "KEY_ALG_RSA_2048"
     *          +"keyOrigin": "GOOGLE_PROVIDED"
     *          +"keyType": "USER_MANAGED"
     *        }
     *        +"status": {
     *          +"code": 200
     *          +"ok": true
     *          +"successful": true
     *          +"failed": false
     *          +"serverError": false
     *          +"clientError": false
     *        }
     *      }
     *      ```
     */
    public function store($iam_email)
    {
        $uri = 'https://iam.googleapis.com/v1/projects/' . $this->project_id . '/serviceAccounts/' . $iam_email . '/keys';
        $response = $this->api_client->rest()->post($uri, [
            'privateKeyType' => 'TYPE_GOOGLE_CREDENTIALS_FILE',
            'keyAlgorithm' => 'KEY_ALG_RSA_2048'
        ]);
        return $response;
    }

    /**
     * Delete a GCP Service Account Key using IAM Email
     *
     * @see https://cloud.google.com/iam/docs/reference/rest/v1/projects.serviceAccounts/delete
     * @uses iam.serviceAccountKeys.delete
     *
     * @param string $iam_email
     *      Service account email address
     *      ```
     *      (example) hackystack-provisioner@my-project.iam.gserviceaccount.com
     *      ```
     *
     * @param string $key
     *      Service account key ID or name
     *      ```
     *      (example) projects/jmartin-92932d23/serviceAccounts/hackystack-unit-test-3625@jmartin-92932d23.iam.gserviceaccount.com/keys/2e063ae7bdefe06ddef4a6ead15aaac6a9cb6320
     *      (example) 2e063ae7bdefe06ddef4a6ead15aaac6a9cb6320
     *      ```
     *
     * @param bool $trim (default: false)
     *      Whether a full key name is provided that needs to be trimmed
     *
     * @return object
     *      ```json
     *      {
     *        +"headers": {
     *          +"Content-Type": "application/json; charset=UTF-8"
     *          +"Vary": "X-Origin Referer Origin,Accept-Encoding"
     *          +"Date": "Tue, 24 May 2022 02:48:38 GMT"
     *          +"Server": "ESF"
     *          +"Cache-Control": "private"
     *          +"X-XSS-Protection": "0"
     *          +"X-Frame-Options": "SAMEORIGIN"
     *          +"X-Content-Type-Options": "nosniff"
     *          +"Alt-Svc": "h3=":443"; ma=2592000,h3-29=":443"; ma=2592000,h3-Q050=":443"; ma=2592000,h3-Q046=":443"; ma=2592000,h3-Q043=":443"; ma=2592000,quic=":443"; ma=2592000; v="46,43""
     *          +"Accept-Ranges": "none"
     *          +"Transfer-Encoding": "chunked"
     *        }
     *        +"json": "[]"
     *        +"object": {}
     *        +"status": {
     *          +"code": 200
     *          +"ok": true
     *          +"successful": true
     *          +"failed": false
     *          +"serverError": false
     *          +"clientError": false
     *        }
     *      }
     *      ```
     */
    public function delete($iam_email, $key, $trim = false)
    {
        if ($trim == true) {
            $parsed_key = substr($key, strrpos($key, '/')+1);
        } else {
            $parsed_key = $key;
        }

        $uri = 'https://iam.googleapis.com/v1/projects/' . $this->project_id . '/serviceAccounts/' . $iam_email . '/keys/' . $parsed_key;
        $response = $this->api_client->rest()->delete($uri, []);
        return $response;
    }
}
