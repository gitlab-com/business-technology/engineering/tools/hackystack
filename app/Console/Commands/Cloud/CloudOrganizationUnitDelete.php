<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudOrganizationUnitDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-organization-unit:delete
                            {short_id? : The short ID of the organization unit.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete a Cloud Organization Unit by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short_id to lookup the record.');
            $this->line('You can get a list of cloud organization units using `cloud-organization-unit:list`');
            $this->line('You can delete by short ID using `cloud-organization-unit:delete a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                ->withCount([
                    'cloudAccounts',
                    'childCloudOrganizationUnits'
                ])->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_organization_unit == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // If cloud accounts are attached to this organization unit, abort deletion
        if($cloud_organization_unit->cloud_accounts_count > 0) {
            $this->error('Error: You cannot delete a organization unit that has cloud accounts attached.');
            $this->line('You can get a list of cloud accounts associated with this organization unit using `cloud-organization-unit:get '.$cloud_organization_unit->short_id.'`');
            $this->error('');
            die();
        }

        // If child organization units are attached to this organization unit, abort deletion
        if($cloud_organization_unit->child_cloud_organization_units_count > 0) {
            $this->error('Error: You cannot delete a organization unit that has child organization units attached.');
            $this->line('You can get a list of cloud organization units associated with this organization unit using `cloud-organization-unit:get '.$cloud_organization_unit->short_id.'`');
            $this->error('');
            die();
        }

        // Cloud Billing Account Values
        $this->comment('');
        $this->comment('Cloud Organization Unit');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_organization_unit->id
                ],
                [
                    'short_id',
                    $cloud_organization_unit->short_id
                ],
                [
                    'auth_tenant_id',
                    $cloud_organization_unit->auth_tenant_id
                ],
                [
                    'cloud_provider_id',
                    $cloud_organization_unit->cloud_provider_id
                ],
                [
                    'cloud_realm_id',
                    $cloud_organization_unit->cloud_realm_id
                ],
                [
                    'parent_cloud_organization_unit',
                    $cloud_organization_unit->cloud_organization_unit_id_parent
                ],
                [
                    'name',
                    $cloud_organization_unit->name
                ],
                [
                    'slug',
                    $cloud_organization_unit->slug
                ],
                [
                    'created_at',
                    $cloud_organization_unit->created_at
                ],
            ]
        );

        $this->comment('');

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $cloudOrganizationUnitService = new Services\V1\Cloud\CloudOrganizationUnitService();

        // Use service to delete record
        $cloudOrganizationUnitService->delete($cloud_organization_unit->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');

    }
}
