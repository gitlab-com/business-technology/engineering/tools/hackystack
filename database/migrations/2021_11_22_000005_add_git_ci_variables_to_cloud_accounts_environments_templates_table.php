<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGitCiVariablesToCloudAccountsEnvironmentsTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('cloud_accounts_environments_templates', 'git_ci_variables')) {
            return;
        }

        Schema::table('cloud_accounts_environments_templates', function (Blueprint $table) {
            $table->json('git_ci_variables')->nullable()->after('git_meta_data');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cloud_accounts_environments_templates', function (Blueprint $table) {
            $table->dropColumn('git_ci_variables');
        });
    }
}
