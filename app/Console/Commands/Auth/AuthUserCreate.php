<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthUserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-user:create
                            {--T|auth_tenant_slug= : The slug of the tenant that users belong to}
                            {--N|full_name= : The first and last name of the user}
                            {--E|email= : The email address of the user}
                            {--P|password= : The password for the user. If not set, a randomly generated password will be created}
                            {--must-change-password : Whether the user will be prompted to change their password when they sign in}
                            {--verified : If user should be allowed to sign in without verifying their email address}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an Authentication User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        //
        // Auth Tenant
        // --------------------------------------------------------------------
        //

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should this group belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Auth Provider
        // --------------------------------------------------------------------
        //

        // Get local provider for tenant
        $auth_provider = Models\Auth\AuthProvider::query()
            ->where('auth_tenant_id', $auth_tenant->id)
            ->where('slug', 'local')
            ->first();

        // Validate that provider exists or return error message
        if(!$auth_provider) {
            $this->error('Error: No local provider was found for that tenant.');
            $this->error('');
            die();
        }

        //
        // Text Fields
        // --------------------------------------------------------------------
        //

        // Full Name
        $full_name = $this->option('full_name');
        if($full_name == null) {
            $full_name = $this->ask('What is the first and last name of the user?');
        }

        // Email
        $email = $this->option('email');
        if($email == null) {
            $email = $this->ask('What is the email address of the user?');
        }

        // Password
        $password = $this->option('password');
        if($password == null) {
            $password = Str::random(12);
        }

        //
        // Boolean Flags
        // --------------------------------------------------------------------
        //

        // Change Password Flag
        if($this->option('must-change-password') == true) {
            $flag_must_change_password = 1;
        } else {
            $flag_must_change_password = 0;
        }

        // Account Verified Flag
        if($this->option('verified') == true) {
            $flag_account_verified = 1;
        } else {
            $flag_account_verified = 0;
        }

        //
        // Validate that existing user does not already exist
        // --------------------------------------------------------------------
        //

        $existing_user = Models\Auth\AuthUser::query()
            ->where('auth_tenant_id', $auth_tenant->id)
            ->where('auth_provider_id', $auth_provider->id)
            ->where('email', $email)
            ->first();

        // Validate that tenant exists or return error message
        if($existing_user) {
            $this->error('Error: A user already exists with that email address');
            $this->comment('You can view the existing user using `auth-user:get '.$existing_user->short_id.'`');
            $this->comment('');
            die();
        }

        //
        // Create User Record
        // --------------------------------------------------------------------
        //

        // Initialize service
        $authUserService = new Services\V1\Auth\AuthUserService();

        // Use service to create record
        $auth_user = $authUserService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'auth_provider_id' => $auth_provider->id,
            'full_name' => $full_name,
            'email' => $email,
            'password' => $password,
            'notify_send_verification_email' => 0,
        ]);

        //
        // Update User Record
        // --------------------------------------------------------------------
        //

        $authUserService->update($auth_user->id, [
            'flag_account_verified' => $flag_account_verified,
            'flag_must_change_password' => $flag_must_change_password,
        ]);

        // Refresh the Eloquent model variable with the latest values
        $auth_user = $auth_user->fresh();

        //
        // Show User Record
        // --------------------------------------------------------------------
        //

        // Auth User Values
        $this->comment('');
        $this->comment('Auth User');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_user->id
                ],
                [
                    'short_id',
                    $auth_user->short_id
                ],
                [
                    'auth_tenant',
                    '['.$auth_user->authTenant->short_id.'] '.$auth_user->authTenant->slug,
                ],
                [
                    'auth_provider',
                    '['.$auth_user->authProvider->short_id.'] '.$auth_user->authProvider->slug,
                ],
                [
                    'full_name',
                    $auth_user->full_name
                ],
                [
                    'email',
                    $auth_user->email
                ],
                [
                    'flag_must_change_password',
                    $auth_user->flag_must_change_password
                ],
                [
                    'flag_account_verified',
                    $auth_user->flag_account_verified
                ],
                [
                    'created_at',
                    $auth_user->created_at->toIso8601String()
                ],
            ]
        );

        $this->info('');
        $this->info('Password: '.$password);
        $this->error('The password will not be shown again. Please copy it to a secure location.');

        $this->comment('');

    }
}
