<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiresAtToCloudAccountsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('expires_at', 'cloud_accounts_users')) {
            return;
        }

        Schema::table('cloud_accounts_users', function (Blueprint $table) {
            $table->timestamp('expires_at')->nullable()->after('provisioned_at');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cloud_accounts_users', function (Blueprint $table) {
            $table->dropColumn('expires_at');
        });
    }
}
