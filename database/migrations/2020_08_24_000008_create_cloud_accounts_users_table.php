<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCloudAccountsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cloud_accounts_users', function (Blueprint $table) {
            $table->uuid('id')->nullable()->index();
            $table->string('short_id', 8)->nullable()->index();
            $table->uuid('auth_tenant_id')->index();
            $table->uuid('auth_user_id')->index();
            $table->uuid('cloud_account_id')->index();
            $table->uuid('cloud_account_group_id')->index()->nullable();
            $table->uuid('cloud_provider_id')->index();
            $table->uuid('cloud_realm_id')->index();
            $table->json('api_meta_data')->nullable();
            $table->string('username')->nullable();
            $table->text('password')->nullable()->comment('encrypted');
            $table->boolean('flag_provisioned')->default(false)->nullable();
            $table->timestamp('provisioned_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->string('state', 55)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cloud_accounts_users');
    }
}
