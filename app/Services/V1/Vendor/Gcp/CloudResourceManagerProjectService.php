<?php

namespace App\Services\V1\Vendor\Gcp;

use App\Services\V1\Vendor\Gcp\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudResourceManagerProjectService
{

    use BaseService;

    /**
     *   Initialize the Google API Client
     *
     *   @see https://developers.google.com/identity/protocols/oauth2/scopes
     *   @see https://github.com/googleapis/google-api-php-client-services/blob/master/src/Google/Service/CloudResourceManager.php
     *
     *   @param uuid    $cloud_provider_id
     */
    public function __construct($cloud_provider_id)
    {
        // Call BaseService methods to establish API connection
        $this->setCloudProvider($cloud_provider_id);
        $this->setGoogleApiClient();

        // Add API Scope(s)
        $this->google_api_client->addScope(\Google_Service_CloudResourceManager::CLOUD_PLATFORM);
    }

    /**
     *   Get an existing project
     *
     *   @see https://github.com/googleapis/google-api-php-client-services/tree/master/src/Google/Service/CloudResourceManager
     *   @see https://cloud.google.com/resource-manager/reference/rest/v3/projects/get
     *   @see https://cloud.google.com/resource-manager/reference/rest/v3/projects#Project
     *
     *   @param  array  $request_data
     *      project_name|string          Ex. projects/123456789012 or 123456789012
     *
     *   @return array                  API Meta Data (Response)
     *      "deleteTime" => null,
     *      "displayName" => "my-project-name-a1b2c3d4",
     *      "etag: "REDACTED==",
     *      "labels: null,
     *      "name: "projects/987654321098",
     *      "parent: "folders/123456789012",
     *      "projectId: "my-project-name-a1b2c3d4",
     *      "state" => "ACTIVE",
     *      "updateTime" => "2021-06-16T02:12:02.779Z"
     */
    public function get($request_data = [])
    {
        // Initialize the Resource Manager API Service
        $google_cloud_resource_manager_service = new \Google_Service_CloudResourceManager($this->google_api_client);

        // Check if project name or ID has required prefix of `projects/`
        if(Str::startsWith($request_data['project_name'], 'projects/')) {
            $project_name = $request_data['project_name'];
        } else {
            $project_name = 'projects/'.$request_data['project_name'];
        }

        // Get the existing project
        try {
            $project_api_response = $google_cloud_resource_manager_service->projects->get($project_name, []);
        } catch(\Google_Service_Exception $e) {
            $this->handleException($e);
        }

        return $project_api_response;
    }

    /**
     *   Provision a Project
     *
     *   @see https://github.com/googleapis/google-api-php-client-services/tree/master/src/Google/Service/CloudResourceManager
     *   @see https://github.com/googleapis/google-api-php-client#making-requests
     *   @see https://cloud.google.com/resource-manager/reference/rest/v3/projects/create
     *   @see https://cloud.google.com/resource-manager/reference/rest/v3/projects#Project
     *
     *   @param  array  $request_data
     *      project_name|string          Ex. my-project-name-a1b2c3d4
     *      parent_folder_name|string    Ex. folders/123456789012
     *      labels|array                 Ex. Array of GCP labels for cost allocation
     *
     *   @return array                  API Meta Data (Response)
     *      "@type" => "type.googleapis.com/google.cloud.resourcemanager.v3.Project",
     *      "name" => "projects/987654321098",
     *      "parent" => "folders/123456789012",
     *      "projectId" => "my-project-name-a1b2c3d4",
     *      "state" => "ACTIVE",
     *      "displayName" => "my-project-name-a1b2c3d4",
     *      "createTime" => "2021-06-09T18:22:19.580Z",
     *      "updateTime" => "2021-06-09T18:22:21.173Z",
     *      "etag" => "REDACTED==",
     *      "labels" => [
     *        "my_label_key" => "my-label-value",
     *      ],
     */
    public function provision($request_data = [])
    {
        // Initialize the Resource Manager API Service
        $google_cloud_resource_manager_service = new \Google_Service_CloudResourceManager($this->google_api_client);

        // Define API request body
        $request_body = new \Google_Service_CloudResourceManager_Project([
            'displayName' => $request_data['project_name'],
            'labels' => !empty($request_data['labels']) ? $request_data['labels'] : null,
            'parent' => $request_data['parent_folder_name'],
            'projectId' => $request_data['project_name']
        ]);

        try {

            // Create the new project
            $project_api_response = $google_cloud_resource_manager_service->projects->create($request_body, []);

            // Uncomment the dd() if you need to troubleshoot the API response
            //dd($project_api_response);

        } catch(\Google_Service_Exception $e) {
            $this->handleException($e);
        }

        // Loop through 1 second intervals (for 30 seconds) until operation status is done
        for($wait_secs = 0; $wait_secs<=30; $wait_secs++) {

            // Perform API call to Operations Service to get status
            $operation_api_response = $google_cloud_resource_manager_service->operations->get($project_api_response->name);

            // If operation has completed, return the response array
            if($operation_api_response->done == true) {

                // Google_Service_CloudResourceManager_Operation {#4138
                //   +done: true,
                //   +metadata: [
                //     "@type" => "type.googleapis.com/google.cloud.resourcemanager.v3.CreateProjectMetadata",
                //     "createTime" => "2021-06-09T18:22:19.580Z",
                //     "gettable" => true,
                //     "ready" => true,
                //   ],
                //   +name: "operations/cp.1234567890123456789",
                //   +response: [
                //     "@type" => "type.googleapis.com/google.cloud.resourcemanager.v3.Project",
                //     "name" => "projects/987654321098",
                //     "parent" => "folders/123456789012",
                //     "projectId" => "my-project-name-a1b2c3d4",
                //     "state" => "ACTIVE",
                //     "displayName" => "my-project-name-a1b2c3d4",
                //     "createTime" => "2021-06-09T18:22:19.580Z",
                //     "updateTime" => "2021-06-09T18:22:21.173Z",
                //     "etag" => "REDACTED==",
                //     "labels" => [
                //       "my_label_key" => "my-label-value",
                //     ],
                //   ],
                // }

                // Return the response subarray
                return $operation_api_response->response;

            }

            // If operation has not completed, sleep and try again
            else {

                // Google_Service_CloudResourceManager_Operation^ {
                //   +done: null
                //   #errorType: "Google_Service_CloudResourceManager_Status"
                //   #errorDataType: ""
                //   +metadata: null
                //   +name: "operations/cf.1234567890123456789"
                //   +response: null
                //   #internal_gapi_mappings: []
                //   #modelData: []
                //   #processed: []
                // }

                if($wait_secs == 30) {
                    abort(504, 'API operation timeout when creating a GCP project', [
                        'project_name' => $request_data['project_name'],
                        'parent_folder_name' => $request_data['parent_folder_name'],
                        'operation_id' => $operation_api_response->name
                    ]);
                    break;
                } else {
                    sleep(1);
                    continue;
                }

            }
        } // for($wait_secs)

    }

    /**
     *   Deprovision a Project
     *
     *   @see https://github.com/googleapis/google-api-php-client-services/tree/master/src/Google/Service/CloudResourceManager
     *   @see https://cloud.google.com/resource-manager/reference/rest/v3/projects/delete
     *
     *   @param  array  $request_data
     *      project_name|string          Ex. projects/987654321098
     *
     *   @return array                  API Meta Data (Response)
     *      "@type" => "type.googleapis.com/google.cloud.resourcemanager.v3.Project"
     *      "name" => "projects/987654321098"
     *      "parent" => "folders/123456789012"
     *      "displayName" => "my-project-name-a1b2c3d4"
     *      "state" => "DELETE_REQUESTED"
     *      "createTime" => "2021-05-21T17:22:08.529Z"
     *      "updateTime" => "2021-05-21T19:39:46.055Z"
     *      "etag" => "REDACTED=="
     */
    public function deprovision($request_data = [])
    {

        // Initialize the Resource Manager API Service
        $google_cloud_resource_manager_service = new \Google_Service_CloudResourceManager($this->google_api_client);

        try {

            // Delete the project
            $project_api_response = $google_cloud_resource_manager_service->projects->delete($request_data['project_name']);

            // Uncomment the dd() if you need to troubleshoot the API response
            //dd($project_api_response);

        } catch(\Google_Service_Exception $e) {
            $this->handleException($e);
        }

        // Loop through 1 second intervals (for 30 seconds) until operation status is done
        for($wait_secs = 0; $wait_secs<=30; $wait_secs++) {

            // Perform API call to Operations Service to get status
            $operation_api_response = $google_cloud_resource_manager_service->operations->get($project_api_response->name);

            // If operation has completed, return the response array
            if($operation_api_response->done == true) {

                // Google_Service_CloudResourceManager_Operation^ {#4131
                //   +done: true
                //   #errorType: "Google_Service_CloudResourceManager_Status"
                //   #errorDataType: ""
                //   +metadata: null
                //   +name: null
                //   +response: array:8 [
                //     "@type" => "type.googleapis.com/google.cloud.resourcemanager.v3.Project"
                //     "name" => "projects/987654321098"
                //     "parent" => "folders/123456789012"
                //     "displayName" => "my-project-name-a1b2c3d4"
                //     "state" => "DELETE_REQUESTED"
                //     "createTime" => "2021-05-21T17:22:08.529Z"
                //     "updateTime" => "2021-05-21T19:39:46.055Z"
                //     "etag" => "REDACTED=="
                //   ]
                //   #internal_gapi_mappings: []
                //   #modelData: []
                //   #processed: []
                // }

                // Return the response subarray
                return $operation_api_response->response;

            }

            // If operation has not completed, sleep and try again
            else {

                // Google_Service_CloudResourceManager_Operation^ {
                //   +done: null
                //   #errorType: "Google_Service_CloudResourceManager_Status"
                //   #errorDataType: ""
                //   +metadata: null
                //   +name: "operations/cf.1234567890123456789"
                //   +response: null
                //   #internal_gapi_mappings: []
                //   #modelData: []
                //   #processed: []
                // }

                if($wait_secs == 30) {
                    abort(504, 'API operation timeout when deleting a GCP project', [
                        'project_name' => $request_data['project_name'],
                        'operation_id' => $operation_api_response->name
                    ]);
                    break;
                } else {
                    sleep(1);
                    continue;
                }

            }
        } // for($wait_secs)

    }

}
