<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthProviderGroupDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-provider-group:delete
                            {short_id? : The short ID of the provider group.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete an Authentication Provider Group by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short_id to lookup the record.');
            $this->line('You can get a list of provider groups using `auth-provider-group:get`');
            $this->line('You can delete by short ID using `auth-provider-group:delete a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_provider_group = Models\Auth\AuthProviderGroup::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($auth_provider_group == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Call the get method to display the tables of values for the record.
        $this->call('auth-provider-group:get', [
            'short_id' => $auth_provider_group->short_id,
        ]);

        $this->comment('');

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $authProviderGroupService = new Services\V1\Auth\AuthProviderGroupService();

        // Use service to delete record
        $authProviderGroupService->delete($auth_provider_group->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');

    }
}
