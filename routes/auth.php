<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

Route::namespace('Auth')->group(function () {

    // Login Form
    Route::get('/login', 'LoginController@show')->name('auth.login');

    // Logout Action
    Route::post('/logout', 'LogoutController@endSession')->name('auth.logout');

    // Local Authentication
    if(config('hackystack.auth.local.enabled') == true) {
        Route::namespace('Local')->group(function () {

            // Local Authentication - Login Form Submission
            Route::post('/login', 'LoginController@authenticate')->name('auth.local.login.authenticate');

            // TODO - 2 Factor Authentication

            // Local Authentication - Register Form
            if(config('hackystack.auth.local.register_enabled') == true) {
                Route::get('/register', 'RegisterController@create')->name('auth.local.register.create');
                Route::post('/register', 'RegisterController@store')->name('auth.local.register.store');
            }

            // Local Authentication - Reset Username or Password Form
            if(config('hackystack.auth.local.reset_enabled') == true) {
                Route::get('/reset', 'ResetController@create')->name('auth.local.reset.create');
                Route::post('/reset', 'ResetController@store')->name('auth.local.reset.store');
                Route::get('/reset/{token}', 'ResetController@edit')->name('auth.local.reset.edit');
                Route::patch('/reset/{token}', 'ResetController@update')->name('auth.local.reset.update');
            }

            // Local Authentication - Verify Email Address Form
            if(config('hackystack.auth.local.verify_enabled') == true) {
                Route::get('/verify', 'VerifyController@show')->name('auth.local.verify.show');
                Route::post('/verify/resend', 'VerifyController@store')->name('auth.local.verify.store');
                Route::post('/verify/{id}/{hash}', 'VerifyController@update')->name('auth.local.verify.update');
            }

        });
    }

    if(env('DB_DATABASE') != null) {

        try {
            // Get list of enabled SSO providers from database
            $enabled_sso_providers = \App\Models\Auth\AuthProvider::query()
                ->where('slug', '!=', 'local')
                ->where('flag_enabled', 1)
                ->get();

            foreach($enabled_sso_providers as $provider) {

                // Define variable to pass into route group function
                $provider_slug = $provider->slug;

                // Dynamically create routes for login and callback for enabled SSO provider
                Route::namespace(Str::title($provider->slug))->group(function () use ($provider_slug) {
                    Route::get('/login/'.$provider_slug, 'LoginController@redirectToProvider')->name('auth.'.$provider_slug.'.login');
                    Route::get('/login/'.$provider_slug.'/callback', 'LoginController@handleProviderCallback')->name('auth.'.$provider_slug.'.callback');
                });

            }
        } catch(\Exception $e) {
            // Ignore exception
        }

    }

});
