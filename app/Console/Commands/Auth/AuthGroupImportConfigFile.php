<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthGroupImportConfigFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-group:import-config-file
                            {--T|auth_tenant_slug= : The slug of the tenant that auth groups belong to}
                            {--F|config_file_name= : The file name (without .php extension) in the config/ directory that you want to import. Default hackystack-auth-groups}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse the config/hackystack-auth-groups.php file and create auth group records.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //
        // Configuration File
        // --------------------------------------------------------------------
        //

        $config_file_name = $this->option('config_file_name');

        // If configuration file was not specified, use default
        if(!$config_file_name) {
            $config_file_name = 'hackystack-auth-groups';
        }

        // Verify that file exists in config/ directory
        if(!config($config_file_name)) {
            $this->error('Error: No file named `config/'.$config_file_name.'.php` was found.');
            $this->error('');
            die();
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Parse Configuration File to Validate Syntax
            // Loop through configuration file array
            foreach(config($config_file_name) as $group) {

                // Add row to console table output
                $preview_groups_table_output[] = [
                    Arr::exists($group, 'id') ? substr($group['id'], 0, 8) : '',
                    'division',
                    $group['slug'],
                ];

                // Loop through subgroups for the group
                foreach($group['groups'] as $subgroup) {

                    // Add row to console table output
                    $preview_groups_table_output[] = [
                        Arr::exists($subgroup, 'id') ? substr($subgroup['id'], 0, 8) : '',
                        'department',
                        $subgroup['slug'],
                    ];

                } // foreach($subgroup)

            } // foreach(config)

            // Show table in console with changes
            $preview_groups_table_headers = ['Existing ID', 'Namespace', 'Group'];
            $this->table($preview_groups_table_headers, $preview_groups_table_output);

            // Ask for confirmation to abort creation.
            if($this->confirm('Do you want to abort the import of these groups?')) {
                $this->error('Error: You aborted. No records were created.');
                die();
            }

        }

        //
        // Auth Tenant
        // --------------------------------------------------------------------
        //

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should these auth groups belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Data File Parsing
        // --------------------------------------------------------------------
        //

        // Initialize service for creating records
        $authGroupService = new Services\V1\Auth\AuthGroupService();
        $authProviderGroupService = new Services\V1\Auth\AuthProviderGroupService();

        // Loop through configuration file array
        foreach(config($config_file_name) as $group) {

            // Lookup auth group
            $auth_group = Models\Auth\AuthGroup::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $group['namespace'].'-'.$group['slug'])
                ->first();

            // If group exists, update the record with any changes
            if($auth_group) {
                // Placeholder for future field matching and updates (beyond the `name` field)

                // Add row to console table output
                $groups_table_output[] = [
                    $auth_group->short_id,
                    $auth_group->namespace,
                    $auth_group->name,
                    $auth_group->slug,
                    '<fg=cyan>exists</>'
                ];
            }

            // Validate that group exists or create a new group
            if(!$auth_group) {

                // Use service to create record
                $auth_group = $authGroupService->store([
                    'auth_tenant_id' => $auth_tenant->id,
                    'name' => $group['name'],
                    'slug' => $group['namespace'].'-'.$group['slug'],
                ]);

                // Add row to console table output
                $groups_table_output[] = [
                    $auth_group->short_id,
                    $auth_group->namespace,
                    $auth_group->name,
                    $auth_group->slug,
                    '<fg=green>created</>'
                ];
            }

            // Check if existing ID is specified in array
            if(Arr::exists($group, 'id')) {

                // Check if ID is different than existing database record
                if($group['id'] != $auth_group->id) {

                    // Validate if UUID is valid format
                    if(!\Ramsey\Uuid\Uuid::isValid($group['id'])) {
                        $this->error('Error: The existing ID provided for the group ('.$group['slug'].') is not formatted as a UUID.');
                        $this->error('');
                        die();
                    }

                    // Update ID on database model
                    $auth_group->id = $group['id'];
                    $auth_group->short_id = substr($group['id'], 0, 8);
                    $auth_group->save();

                    // Refresh database model for variable use later in this method
                    $auth_group = $auth_group->fresh();

                    // Add row to console table output
                    $groups_table_output[] = [
                        $auth_group->short_id,
                        $auth_group->namespace,
                        $auth_group->name,
                        $auth_group->slug,
                        '<fg=yellow>existing id changed</>'
                    ];

                } // if($group['id'] !=)
            } // if(Arr::exists(id))

            // Loop through provider groups for the subgroup
            foreach($group['providers'] as $provider_slug => $provider_groups) {

                // Lookup provider
                $auth_provider = Models\Auth\AuthProvider::query()
                    ->where('auth_tenant_id', $auth_tenant->id)
                    ->where('slug', $provider_slug)
                    ->first();

                // If provider was not found, skip creation of group and show an
                // error message but fail gracefully (do not stop script)
                if(!$auth_provider) {
                    $this->error('Error: The authentication provider ('.$provider_slug.') was not found for this tenant ('.$auth_tenant->short_id.').');
                    $this->error('The authentication provider groups for this group ('.$subgroup['slug'].') have not been created.');
                    $this->error('');
                    die();
                }

                // If provider was found, create provider groups
                else {

                    // If this group should be added for all users that authenticate
                    // using this provider. This value is set in the array using:
                    // {group-slug}.providers.{provider-slug}.default = true|false
                    if($provider_groups['default'] == true) {

                        // Check if provider group has already been created
                        $auth_provider_group = Models\Auth\AuthProviderGroup::query()
                            ->where('auth_tenant_id', $auth_tenant->id)
                            ->where('auth_provider_id', $auth_provider->id)
                            ->where('auth_group_id', $auth_group->id)
                            ->where('type', 'default')
                            ->first();

                        // If provider group does not exist yet
                        if(!$auth_provider_group) {

                            // Create auth provider group using service method
                            $authProviderGroupService->store([
                                'auth_tenant_id' => $auth_tenant->id,
                                'auth_provider_id' => $auth_provider->id,
                                'auth_group_id' => $auth_group->id,
                                'type' => 'default'
                            ]);

                        } // if(!$auth_provider_group)

                    } // foreach($provider_groups['default'])

                    // Loop through all meta key/values
                    foreach($provider_groups['meta'] as $meta_group) {

                        // Check if provider group has already been created
                        $auth_provider_group = Models\Auth\AuthProviderGroup::query()
                            ->where('auth_tenant_id', $auth_tenant->id)
                            ->where('auth_provider_id', $auth_provider->id)
                            ->where('auth_group_id', $auth_group->id)
                            ->where('type', 'meta')
                            ->where('meta_key', $meta_group['meta_key'])
                            ->where('meta_value', $meta_group['meta_value'])
                            ->first();

                        // If provider group does not exist yet
                        if(!$auth_provider_group) {

                            // Create auth provider group using service method
                            $authProviderGroupService->store([
                                'auth_tenant_id' => $auth_tenant->id,
                                'auth_provider_id' => $auth_provider->id,
                                'auth_group_id' => $auth_group->id,
                                'type' => 'meta',
                                'meta_key' => $meta_group['meta_key'],
                                'meta_value' => $meta_group['meta_value'],
                            ]);

                        } // if(!$auth_provider_group)

                    } // foreach($provider_groups['meta'])
                } // else
            } // foreach($group['providers'])

            // Loop through sub groups for the group
            foreach($group['groups'] as $subgroup) {

                // Lookup auth group
                $auth_group = Models\Auth\AuthGroup::query()
                    ->where('auth_tenant_id', $auth_tenant->id)
                    ->where('slug', $subgroup['namespace'].'-'.$subgroup['slug'])
                    ->first();

                // If group exists, update the record with any changes
                if($auth_group) {
                    // Placeholder for future field matching and updates (beyond the `name` field)

                    // Add row to console table output
                    $groups_table_output[] = [
                        $auth_group->short_id,
                        $auth_group->namespace,
                        $auth_group->name,
                        $auth_group->slug,
                        '<fg=cyan>exists</>'
                    ];
                }

                // Validate that group exists or create a new group
                if(!$auth_group) {

                    // Use service to create record
                    $auth_group = $authGroupService->store([
                        'auth_tenant_id' => $auth_tenant->id,
                        'name' => $subgroup['name'],
                        'slug' => $subgroup['namespace'].'-'.$subgroup['slug'],
                    ]);

                    // Add row to console table output
                    $groups_table_output[] = [
                        $auth_group->short_id,
                        $auth_group->namespace,
                        $auth_group->name,
                        $auth_group->slug,
                        '<fg=green>created</>'
                    ];
                }

                // Check if existing ID is specified in array
                if(Arr::exists($subgroup, 'id')) {

                    // Check if ID is different than existing database record
                    if($subgroup['id'] != $auth_group->id) {

                        // Validate if UUID is valid format
                        if(!\Ramsey\Uuid\Uuid::isValid($subgroup['id'])) {
                            $this->error('Error: The existing ID provided for the group ('.$subgroup['slug'].') is not formatted as a UUID.');
                            $this->error('');
                            die();
                        }

                        // Update ID on database model
                        $auth_group->id = $subgroup['id'];
                        $auth_group->short_id = substr($subgroup['id'], 0, 8);
                        $auth_group->save();

                        // Refresh database model for variable use later in this method
                        $auth_group = $auth_group->fresh();

                        // Add row to console table output
                        $groups_table_output[] = [
                            $auth_group->short_id,
                            $auth_group->namespace,
                            $auth_group->name,
                            $auth_group->slug,
                            '<fg=yellow>existing id changed</>'
                        ];

                    } // if($group['id'] !=)
                } // if(Arr::exists(id))

                // Loop through provider groups for the subgroup
                foreach($subgroup['providers'] as $provider_slug => $provider_groups) {

                    // Lookup provider
                    $auth_provider = Models\Auth\AuthProvider::query()
                        ->where('auth_tenant_id', $auth_tenant->id)
                        ->where('slug', $provider_slug)
                        ->first();

                    // If provider was not found, skip creation of group and show an
                    // error message but fail gracefully (do not stop script)
                    if(!$auth_provider) {
                        $this->error('Error: The authentication provider ('.$provider_slug.') was not found for this tenant ('.$auth_tenant->short_id.').');
                        $this->error('The authentication provider groups for this group ('.$subgroup['slug'].') have not been created.');
                        $this->error('');
                        die();
                    }

                    // If provider was found, create provider groups
                    else {

                        // If this group should be added for all users that authenticate
                        // using this provider. This value is set in the array using:
                        // {group-slug}.providers.{provider-slug}.default = true|false
                        if($provider_groups['default'] == true) {

                            // Check if provider group has already been created
                            $auth_provider_group = Models\Auth\AuthProviderGroup::query()
                                ->where('auth_tenant_id', $auth_tenant->id)
                                ->where('auth_provider_id', $auth_provider->id)
                                ->where('auth_group_id', $auth_group->id)
                                ->where('type', 'default')
                                ->first();

                            // If provider group does not exist yet
                            if(!$auth_provider_group) {

                                // Create auth provider group using service method
                                $authProviderGroupService->store([
                                    'auth_tenant_id' => $auth_tenant->id,
                                    'auth_provider_id' => $auth_provider->id,
                                    'auth_group_id' => $auth_group->id,
                                    'type' => 'default'
                                ]);

                            } // if(!$auth_provider_group)

                        } // foreach($provider_groups['default'])

                        // Loop through all meta key/values
                        foreach($provider_groups['meta'] as $meta_group) {

                            // Check if provider group has already been created
                            $auth_provider_group = Models\Auth\AuthProviderGroup::query()
                                ->where('auth_tenant_id', $auth_tenant->id)
                                ->where('auth_provider_id', $auth_provider->id)
                                ->where('auth_group_id', $auth_group->id)
                                ->where('type', 'meta')
                                ->where('meta_key', $meta_group['meta_key'])
                                ->where('meta_value', $meta_group['meta_value'])
                                ->first();

                            // If provider group does not exist yet
                            if(!$auth_provider_group) {

                                // Create auth provider group using service method
                                $authProviderGroupService->store([
                                    'auth_tenant_id' => $auth_tenant->id,
                                    'auth_provider_id' => $auth_provider->id,
                                    'auth_group_id' => $auth_group->id,
                                    'type' => 'meta',
                                    'meta_key' => $meta_group['meta_key'],
                                    'meta_value' => $meta_group['meta_value'],
                                ]);

                            } // if(!$auth_provider_group)

                        } // foreach($provider_groups['meta'])
                    } // else
                } // foreach($subgroup['providers'])

            } // foreach($group)

        } // foreach(config)

        // Show table in console with changes
        $groups_table_headers = ['ID', 'Namespace', 'Group Name', 'Slug', 'State'];
        $this->table($groups_table_headers, $groups_table_output);

    } // handle()

}
