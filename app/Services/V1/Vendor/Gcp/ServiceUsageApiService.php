<?php

namespace App\Services\V1\Vendor\Gcp;

use Exception;
use Glamstack\GoogleCloud\ApiClient;

class ServiceUsageApiService
{
    use BaseService;

    const API_SCOPES = [
        'https://www.googleapis.com/auth/cloud-platform'
    ];

    private ApiClient $api_client;

    /**
     * Service Usage API Service
     *
     * @param string $cloud_provider_id
     *      The UUID of the GCP Cloud Provider
     *
     * @param string $project_id
     *      This is the api_meta_data['name'] in the CloudAccount record
     *      ```
     *      projects/123456789012
     *      ```
     */
    public function __construct(string $cloud_provider_id, string $project_id)
    {
        $this->setCloudProvider($cloud_provider_id);

        $this->setJsonKeyFilePath();

        $this->setProjectId($project_id);

        $this->api_client = $this->setGlamstackGoogleCloudSdk(
            self::API_SCOPES,
            $this->json_key_file_path,
            $this->project_id
        );
    }

    /**
     * Validate if service usage API is enabled or return abort message
     *
     * @return bool
     *      True if enabled
     *      False if not enabled
     */
    public function checkIfServiceUsageApiEnabled()
    {
        $response = $this->api_client->rest()->get(
            'https://serviceusage.googleapis.com/v1/' . $this->project_id . '/services'
        );

        if ($response->status->ok) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get a list of enabled API services
     *
     * @see https://cloud.google.com/service-usage/docs/reference/rest/v1/services/list
     *
     * @return object
     */
    public function listEnabledServices()
    {
        $response = $this->api_client->rest()->get(
            'https://serviceusage.googleapis.com/v1/' . $this->project_id . '/services',
            ['filter' => 'state:ENABLED']
        );

        if ($response->status->ok == true) {
            return $response;
        } else {
            if (property_exists($response->object, 'error')) {
                throw new Exception($response->object->error->message);
            } else {
                throw new Exception($response->json);
            }
        }
    }

    /**
     * Get array of enabled API service names
     *
     * If services is not included in the API response, then the GCP project is
     * still initializing and an empty array is returned.
     *
     * @return array
     *      [
     *          0 => "admin.googleapis.com"
     *          1 => "autoscaling.googleapis.com"
     *          2 => "bigquery.googleapis.com"
     *          3 => "bigquerymigration.googleapis.com"
     *          4 => "bigquerystorage.googleapis.com"
     *          5 => "calendar-json.googleapis.com"
     *          6 => "compute.googleapis.com"
     *          7 => "container.googleapis.com"
     *          8 => "containerfilesystem.googleapis.com"
     *          9 => "containerregistry.googleapis.com"
     *          10 => "deploymentmanager.googleapis.com"
     *          11 => "dns.googleapis.com"
     *          12 => "drive.googleapis.com"
     *          13 => "gmail.googleapis.com"
     *          14 => "groupssettings.googleapis.com"
     *          15 => "iam.googleapis.com"
     *          16 => "iamcredentials.googleapis.com"
     *          17 => "monitoring.googleapis.com"
     *          18 => "oslogin.googleapis.com"
     *          19 => "people.googleapis.com"
     *          20 => "pubsub.googleapis.com"
     *          21 => "runtimeconfig.googleapis.com"
     *          22 => "serviceusage.googleapis.com"
     *          23 => "storage-api.googleapis.com"
     *          24 => "websecurityscanner.googleapis.com"
     *      ]
     */
    public function listEnabledServicesArray()
    {
        $response = $this->listEnabledServices();

        if (property_exists($response->object, 'services')) {
            foreach ($response->object->services as $service) {
                $services_array[] = $service->config->name;
            }

            return $services_array;
        } else {
            return [];
        }
    }

    /**
     * Check if service is enabled
     *
     * @param string $domain
     *      The name of the API (FQDN with `googleapis.com` suffix)
     *      ```
     *      compute.googleapis.com
     *      ```
     *
     * @return bool
     *      True if enabled
     *      False if not enabled
     */
    public function checkIfServiceEnabled($domain)
    {
        $services_array = $this->listEnabledServicesArray();

        if (in_array($domain, $services_array)) {
            return true;
        } else {
            return false;
        }
    }
}
