<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthTenantDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-tenant:delete
                            {short_id? : The short ID of the tenant.}
                            {--slug= : The slug of the tenant.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete an Authentication Tenant by ID or slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug
        $slug = $this->option('slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $slug == null) {
            $this->error('You did not specify the short_id or slug to lookup the record.');
            $this->comment('You can lookup by short ID using `auth-tenant:delete a1b2c3d4`');
            $this->comment('You can lookup by slug using `auth-tenant:delete --slug=example`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($slug != null) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $slug)
                ->first();
        }

        // If record not found, return an error message
        if($auth_tenant == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Call the get method to display the tables of values for the record.
        $this->call('auth-tenant:get', [
            'short_id' => $auth_tenant->short_id,
            '--without-child-relationships' => true
        ]);

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $authTenantService = new Services\V1\Auth\AuthTenantService();

        // Use service to delete record
        $authTenantService->delete($auth_tenant->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');

    }
}
