<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthProviderFieldImportConfigFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-provider-field:import-config-file
                            {--T|auth_tenant_slug= : The slug of the tenant that auth provider fields belong to}
                            {--F|config_file_name= : The file name (without .php extension) in the config/ directory that you want to import. Default hackystack-auth-provider-fields}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse the config/hackystack-auth-provider-fields.php file and create auth provider field records.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //
        // Configuration File
        // --------------------------------------------------------------------
        //

        $config_file_name = $this->option('config_file_name');

        // If configuration file was not specified, use default
        if(!$config_file_name) {
            $config_file_name = 'hackystack-auth-provider-fields';
        }

        // Verify that file exists in config/ directory
        if(!config($config_file_name)) {
            $this->error('Error: No file named `config/'.$config_file_name.'.php` was found.');
            $this->error('');
            die();
        }

        //
        // Auth Tenant
        // --------------------------------------------------------------------
        //

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should these auth provider fields belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Data File Parsing
        // --------------------------------------------------------------------
        //

        // Initialize service for creating records
        $authProviderFieldService = new Services\V1\Auth\AuthProviderFieldService();

        // Loop through configuration file array
        foreach(config($config_file_name) as $provider => $fields) {

            // Lookup auth provider
            $auth_provider = Models\Auth\AuthProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $provider)
                ->first();

            // Validate that provider exists or return error message
            if(!$auth_provider) {
                $this->error('Error: No provider was found with the slug (`'.$provider.'`).');
                $this->error('You can get a list of available providers with `auth-provider:list`');
                $this->error('');
                die();
            }

            foreach($fields as $field) {

                // Lookup auth provider field
                $auth_provider_field = Models\Auth\AuthProviderField::query()
                    ->where('auth_provider_id', $auth_provider->id)
                    ->where('provider_key', $field['provider_key'])
                    ->where('user_column', $field['user_column'])
                    ->first();

                // If provider field exists, update the record with any changes
                if($auth_provider_field) {
                    // Placeholder for future field matching and updates (beyond the `name` field)

                    // Add row to console table output
                    $fields_table_output[] = [
                        $auth_provider_field->short_id,
                        $auth_provider_field->authProvider->slug,
                        $auth_provider_field->provider_key,
                        $auth_provider_field->user_column,
                        '<fg=cyan>exists</>'
                    ];
                }

                // Validate that provider key exists or create a new group
                if(!$auth_provider_field) {

                    // Use service to create record
                    $auth_provider_field = $authProviderFieldService->store([
                        'auth_tenant_id' => $auth_tenant->id,
                        'auth_provider_id' => $auth_provider->id,
                        'provider_key' => $field['provider_key'],
                        'user_column' => $field['user_column']
                    ]);

                    // Add row to console table output
                    $fields_table_output[] = [
                        $auth_provider_field->short_id,
                        $auth_provider_field->authProvider->slug,
                        $auth_provider_field->provider_key,
                        $auth_provider_field->user_column,
                        '<fg=green>created</>'
                    ];
                }

            } // foreach($fields)
        } // foreach(config)

        // Show table in console with changes
        $fields_table_headers = ['ID', 'Provider', 'Provider Key', 'User Column', 'State'];
        $this->table($fields_table_headers, $fields_table_output);

    } // handle()

}
