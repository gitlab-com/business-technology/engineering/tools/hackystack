<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CloudOrganizationUnitCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-organization-unit:create
                            {--T|auth_tenant_slug= : The slug of the tenant this organization unit belongs to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider this organization unit belongs to}
                            {--R|cloud_realm_slug= : The slug of the cloud realm this organization unit belongs to}
                            {--O|cloud_organization_unit_parent_slug= : The slug of the parent cloud organization unit that this organization unit is nested in}
                            {--N|name= : The display name of this organization unit}
                            {--S|slug= : The alpha-dash shorthand name for this organization unit}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Cloud Organization Unit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Organization Units - Create Record');

        //
        // Auth Tenant
        // --------------------------------------------------------------------
        //

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should this organization unit belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Provider
        // --------------------------------------------------------------------
        //

        // Get provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // If provider slug option was specified, lookup by slug
        if($cloud_provider_slug) {
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_slug)
                ->first();
        }

        // If provider slug was not provided, prompt for input
        else {

            // Get list of providers to show in console
            $cloud_providers = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->line('');
            $this->line('Available cloud providers: '.implode(',', Arr::flatten($cloud_providers)));

            $cloud_provider_prompt = $this->anticipate('Which cloud provider should this organization unit belong to?', Arr::flatten($cloud_providers));

            // Lookup cloud provider based on slug provided in prompt
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$cloud_provider) {
            $this->error('Error: No cloud provider was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Realm
        // --------------------------------------------------------------------
        //

        // Get realm slug
        $cloud_realm_slug = $this->option('cloud_realm_slug');

        // If realm slug option was specified, lookup by slug
        if($cloud_realm_slug) {
            $cloud_realm = Models\Cloud\CloudRealm::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('slug', $cloud_realm_slug)
                ->first();
        }

        // If realm slug was not provided, prompt for input
        else {

            // Get list of providers to show in console
            $cloud_realms = Models\Cloud\CloudRealm::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->get(['slug'])
                ->toArray();

            // If no billing accounts exist, then return an error
            if(count($cloud_realms) == 0) {
                $this->error('Error: No cloud realms were found for the tenant and provider.');
                $this->comment('Please create a cloud realm before creating an organization unit.');
                $this->line('cloud-realm:create');
                $this->error('');
                die();
            }

            $this->line('');
            $this->line('Available cloud realms: '.implode(',', Arr::flatten($cloud_realms)));

            $cloud_realm_prompt = $this->anticipate('Which cloud realm should this organization unit belong to?', Arr::flatten($cloud_realms));

            // Lookup cloud provider based on slug provided in prompt
            $cloud_realm = Models\Cloud\CloudRealm::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('slug', $cloud_realm_prompt)
                ->firstOrFail();

        }

        // Validate that realm exists or return error message
        if(!$cloud_realm) {
            $this->error('Error: No cloud realm was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Parent Cloud Organization Unit
        // --------------------------------------------------------------------
        //

        // Get realm slug
        $cloud_organization_unit_parent_slug = $this->option('cloud_organization_unit_parent_slug');

        // If realm slug option was specified, lookup by slug
        if($cloud_organization_unit_parent_slug) {
            $parent_cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_realm_id', $cloud_realm->id)
                ->where('slug', $cloud_organization_unit_parent_slug)
                ->first();
        }

        // If organization unit slug was not provided, prompt for input
        else {

            // Get list of organization unit to show in console
            $cloud_organization_units = Models\Cloud\CloudOrganizationUnit::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_realm_id', $cloud_realm->id)
                ->get(['slug'])
                ->toArray();

            // Available cloud organization units
            $this->table(
                ['Organization Unit Slug'],
                $cloud_organization_units
            );

            $cloud_organization_unit_prompt = $this->anticipate('Which parent cloud organization unit should this organization unit belong to?', Arr::flatten($cloud_organization_units));

            // Lookup cloud organization unit based on slug provided in prompt
            $parent_cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_realm_id', $cloud_realm->id)
                ->where('slug', $cloud_organization_unit_prompt)
                ->firstOrFail();

        }

        // Validate that realm exists or return error message
        if(!$parent_cloud_organization_unit) {
            $this->error('Error: No parent cloud organization unit was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Text Fields
        // --------------------------------------------------------------------
        //

        // Name
        $name = $this->option('name');
        if($name == null) {
            $name = $this->ask('What is the display name of this cloud organization unit?');
        }

        // Slug
        $slug = $this->option('slug');
        if($slug == null) {
            $slug = $this->ask('What is the alpha-dash shorthand name for this cloud organization unit?');
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'auth_tenant_id',
                        '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                    ],
                    [
                        'cloud_provider_id',
                        '['.$cloud_provider->short_id.'] '.$cloud_provider->slug
                    ],
                    [
                        'cloud_realm_id',
                        '['.$cloud_realm->short_id.'] '.$cloud_realm->slug
                    ],
                    [
                        'cloud_organization_unit_id_parent',
                        '['.$parent_cloud_organization_unit->short_id.'] '.$parent_cloud_organization_unit->slug
                    ],
                    [
                        'name',
                        $name
                    ],
                    [
                        'slug',
                        $slug
                    ],
                ]
            );

            // Ask for confirmation to abort creation.
            if($this->confirm('Do you want to abort the creation of the record?')) {
                $this->error('Error: You aborted. No record was created.');
                die();
            }

        }

        // Initialize service
        $cloudOrganizationUnitService = new Services\V1\Cloud\CloudOrganizationUnitService();

        // Use service to create record
        $record = $cloudOrganizationUnitService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'cloud_organization_unit_id_parent' => $parent_cloud_organization_unit->id,
            'cloud_provider_id' => $cloud_provider->id,
            'cloud_realm_id' => $cloud_realm->id,
            'name' => $name,
            'slug' => $slug,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('cloud-organization-unit:get', [
            'short_id' => $record->short_id
        ]);

    }

}
