<?php

namespace App\Services\V1\Vendor\Aws;

use App\Models;
use Illuminate\Support\Str;

class BaseService
{

    protected $cloud_provider;
    protected $aws_api_client;

    public function setCloudProvider($cloud_provider_id)
    {
        // Get cloud provider from database
        $this->cloud_provider = Models\Cloud\CloudProvider::query()
            ->where('id', $cloud_provider_id)
            ->firstOrFail();

        // Check if cloud provider type is not correct
        if($this->cloud_provider->type != 'aws') {
            abort(500, 'The AWS service is trying to use a cloud provider that is not the correct type.', [
                'cloud_provider_id' => $this->cloud_provider->id,
                'cloud_provider_type' => $this->cloud_provider->type,
            ]);
        }
    }

    public function handleException($e)
    {
        $error_message = json_decode($e->getMessage());
        dd($error_message->error->code.' '.$error_message->error->message);
    }

}
