<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudAccountDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account:delete
                            {short_id? : The short ID of the cloud account.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete a Cloud Account by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short_id to lookup the record.');
            $this->line('You can get a list of cloud accounts using `cloud-account:list`');
            $this->line('You can delete by short ID using `cloud-account:delete a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_account == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Cloud Billing Account Values
        $this->comment('');
        $this->comment('Cloud Account');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_account->id
                ],
                [
                    'short_id',
                    $cloud_account->short_id
                ],
                [
                    'auth_tenant_id',
                    $cloud_account->auth_tenant_id
                ],
                [
                    'cloud_provider_id',
                    $cloud_account->cloud_provider_id
                ],
                [
                    'cloud_realm_id',
                    $cloud_account->cloud_realm_id
                ],
                [
                    'name',
                    $cloud_account->name
                ],
                [
                    'slug',
                    $cloud_account->slug
                ],
                [
                    'created_at',
                    $cloud_account->created_at
                ],
            ]
        );

        $this->comment('');

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $cloudAccountService = new Services\V1\Cloud\CloudAccountService();

        // Use service to delete record
        $cloudAccountService->delete($cloud_account->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');

    }
}
