<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CloudAccountEnvironmentTemplateGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment-template:get
                            {short_id? : The short ID of the environment template.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a Cloud Account Environment Template by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if ($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of accounts using `cloud-account-environment-template:list`');
            $this->line('You can lookup by short ID using `cloud-account-environment-template:get a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif ($short_id != null) {
            $cloud_account_environment_template = Models\Cloud\CloudAccountEnvironmentTemplate::with([
                'authTenant',
                'cloudProvider',
                // 'cloudAccountEnvironments'
            ])->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if ($cloud_account_environment_template == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Cloud Account Values
        $this->comment('');
        $this->comment('Cloud Account Environment Template');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_account_environment_template->id
                ],
                [
                    'short_id',
                    $cloud_account_environment_template->short_id
                ],
                [
                    'auth_tenant_id',
                    '[' . $cloud_account_environment_template->authTenant->short_id . '] ' . $cloud_account_environment_template->authTenant->slug
                ],
                [
                    'cloud_provider_id',
                    '[' . $cloud_account_environment_template->cloudProvider->short_id . '] ' . $cloud_account_environment_template->cloudProvider->slug
                ],
                [
                    'git_project_id',
                    $cloud_account_environment_template->git_project_id
                ],
                [
                    'git_name',
                    $cloud_account_environment_template->git_name ? $cloud_account_environment_template->git_name : 'n/a'
                ],
                [
                    'git_description',
                    $cloud_account_environment_template->git_description ? Str::limit($cloud_account_environment_template->git_description, 50, '...') : 'n/a'
                ],
                [
                    'name',
                    $cloud_account_environment_template->name
                ],
                [
                    'description',
                    $cloud_account_environment_template->description
                ],
                [
                    'state',
                    $cloud_account_environment_template->state
                ]
            ]
        );

        if ($cloud_account_environment_template->git_meta_data != null) {

            $git_meta_data_values = [];
            foreach ($cloud_account_environment_template->git_meta_data as $meta_data_key => $meta_data_value) {
                $git_meta_data_values[] = [
                    $meta_data_key,
                    Str::limit(is_array($meta_data_value) ? json_encode($meta_data_value) : $meta_data_value, 100, '...')
                ];
            }

            $this->newLine();
            $this->table(
                ['Git Meta Data Column', 'API Value'],
                $git_meta_data_values
            );
            $this->newLine();
        } else {

            $this->newLine();
            $this->line('<fg=red>Git Meta Data is empty. Check logs for errors with the CloudAccountEnvironmentTemplate::updateApiMetaData method</>');
            $this->newLine();
        }

        // TODO Fix order of values from array
        $this->comment('CI Variables');

        if ($cloud_account_environment_template->git_ci_variables != null && $cloud_account_environment_template->git_ci_variables != []) {
            $this->table(
                ['key', 'value', 'masked', 'protected', 'dynamic_type', 'variable_type', 'environment_scope'],
                $cloud_account_environment_template->git_ci_variables
            );
            $this->newLine();
        } else {
            $this->line('<fg=red>No CI variables have been created. Use `cloud-account-environment-template:create-ci-variable ' . $cloud_account_environment_template->short_id . '` to add a new variable.</>');
        }

        //
        // Child Relationship - Cloud Account Environments
        // --------------------------------------------------------------------
        // Loop through cloud account environments in Eloquent model and add
        // calculated values to array. The dot notation for pivot relationships
        // doesn't work with a get([]) so this is the best way to handle this.
        //
        /*
        // Loop through records and add values to array
        $cloud_account_environments_rows = [];
        foreach($cloud_account_environment_templates->cloudAccountEnvironments as $environment) {
            $cloud_account_environments_rows[] = [
                'short_id' => $environment->short_id,
                'cloud_account' => '['.$environment->cloudAccount->short_id.'] '.$environment->cloudAccount->slug,
                'git_project_id' => $environment->git_name,
                'name' => $environment->name,
                'state' => $environment->state,
                'created_at' => $environment->created_at->format('Y-m-d')
            ];
        }

        $this->comment('');
        $this->comment('Cloud Account Environments');

        // If rows exists, render a table or return a no results found message
        if(count($cloud_account_environment_rows) > 0) {
            $this->table(
                ['Environment Short ID', 'Cloud Account', 'API Project ID', 'Name', 'State', 'Created'],
                $cloud_account_role_rows
            );
        } else {
            $this->error('No cloud account environments have been created.');
        }

        $this->newLine();
*/
    }
}
