<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class AuthTenantCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-tenant:create
                            {--N|name= : The display name of this tenant}
                            {--S|slug= : The alpha-dash shorthand name for this tenant}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an Authentication Tenant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Auth Tenants - Create Record');

        // Name
        $name = $this->option('name');
        if($name == null) {
            $name = $this->ask('What is the display name of this tenant?');
        }

        // Slug
        $slug = $this->option('slug');
        if($slug == null) {
            $slug = $this->ask('What is the alpha-dash shorthand name for this tenant?');
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'name',
                        $name
                    ],
                    [
                        'slug',
                        $slug
                    ],
                ]
            );

            // Ask for confirmation to abort creation.
            if($this->confirm('Do you want to abort the creation of the record?')) {
                $this->error('Error: You aborted. No record was created.');
                die();
            }

        }

        // Initialize service
        $authTenantService = new Services\V1\Auth\AuthTenantService();

        // Use service to create record
        $record = $authTenantService->store([
            'name' => $name,
            'slug' => $slug,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('auth-tenant:get', [
            'short_id' => $record->short_id
        ]);

    }

}
