<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudBillingAccountDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-billing-account:delete
                            {short_id? : The short ID of the billing account.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete a Cloud Billing Account by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short_id to lookup the record.');
            $this->line('You can get a list of cloud billing accounts using `cloud-billing-account:list`');
            $this->line('You can delete by short ID using `cloud-billing-account:delete a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_billing_account = Models\Cloud\CloudBillingAccount::query()
                ->withCount([
                    'cloudAccounts',
                    'cloudRealms',
                ])->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_billing_account == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // If cloud accounts are attached to this realm, abort deletion
        if($cloud_billing_account->cloud_accounts_count > 0) {
            $this->error('Error: You cannot delete a billing account that has cloud accounts attached.');
            $this->line('You can get a list of cloud accounts associated with this billing account using `cloud-billing-account:get '.$cloud_realm->short_id.'`');
            $this->error('');
            die();
        }

        // Cloud Billing Account Values
        $this->comment('');
        $this->comment('Cloud Billing Account');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_billing_account->id
                ],
                [
                    'short_id',
                    $cloud_billing_account->short_id
                ],
                [
                    'auth_tenant_id',
                    $cloud_billing_account->auth_tenant_id
                ],
                [
                    'cloud_provider_id',
                    $cloud_billing_account->cloud_provider_id
                ],
                [
                    'name',
                    $cloud_billing_account->name
                ],
                [
                    'slug',
                    $cloud_billing_account->slug
                ],
                [
                    'created_at',
                    $cloud_billing_account->created_at
                ],
            ]
        );

        $this->comment('');

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $cloudBillingAccountService = new Services\V1\Cloud\CloudBillingAccountService();

        // Use service to delete record
        $cloudBillingAccountService->delete($cloud_billing_account->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');

    }
}
