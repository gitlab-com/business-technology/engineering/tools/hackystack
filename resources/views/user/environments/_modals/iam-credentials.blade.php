<div class="modal fade" id="iam-credentials" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">IAM Credentials</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">GitOps URL</label>
                    <div class="col-md-9 col-form-label">
                        @if($request->user()->authTenant->git_provider_base_url)
                            <a target="_blank" href="{{ $request->user()->authTenant->git_provider_base_url }}">{{ $request->user()->authTenant->git_provider_base_url }}</a>
                        @else
                            The GitOps instance has not been provisioned. Please contact the System Administrator for assistance.
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">Username</label>
                    <div class="col-md-9 col-form-label">
                        @if($request->user()->git_username != null && $request->user()->flag_git_user_provisioned == true)
                            <code>{{ $request->user()->git_username }}</code>
                        @else
                            Your GitOps user account has not been provisioned yet.
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">Password</label>
                    <div class="col-md-9 col-form-label">
                        @if($request->user()->git_username != null && $request->user()->flag_git_user_provisioned == true)
                            <code>{{ decrypt($request->user()->git_password) }}</code>
                        @else
                            Your GitOps user account has not been provisioned yet.
                        @endif
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
