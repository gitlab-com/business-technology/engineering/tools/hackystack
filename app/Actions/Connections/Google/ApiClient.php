<?php

namespace App\Actions\Connections\Google;

use App\Actions\Connections\Google\ApiToken;
use App\Actions\Connections\Google\Exceptions\ConfigurationException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

/**
 * Google API Client for OAUTH Client and GCP Service Account JSON Keys
 *
 * @author Dillon Wheeler
 * @author Jeff Martin
 *
 * This is used to perform Laravel HTTP Client REST API calls to any of Google's service APIs.
 *
 * You can configure your connections (ex. `workspace`, `cloud`, etc.) in the `config/connections.php` and `.env` file.
 *
 * To provide a streamlined developer experience, this uses either the json_key_file_path parameter that points
 * to your JSON API key's storage path, or you can provide the JSON API key as a string in the json_key parameter.
 *
 * All authentication is performed in the `ApiToken` class that will generate a short-lived bearer token.
 *
 * @see https://developers.google.com/apis-explorer
 */
class ApiClient
{
    /**
     * Google API Get Request
     *
     * This method is called from other services to perform a GET request and return a structured object.
     *
     * Example Usage:
     * ```php
     * $response = ApiClient::get($connection_key, 'https://admin.googleapis.com/admin/directory/v1/groups', [], ['customer']);
     * ```
     *
     * @param ?string $connection_key
     *      The connection key in config/connections.php. Set to null to use default connection key.
     *
     * @param string $url
     *      The full URL of the API endpoint
     *
     * @param array $query_data
     *      Array of query data to apply to GET request
     *
     * @param array $connection_config_query_keys
     *      Array of connection configuration keys that should be included with API requests for specific endpoints.
     *      This is mostly used for Google Workspace related API endpoints.
     *      Ex. customer, customer_id, domain, subject_email
     *
     * @return object
     *      See parseApiResponse() method. The content and schema of the object and json arrays can be found in the REST
     *      API documentation for the specific endpoint.
     */
    public static function get(
        ?string $connection_key,
        string $url,
        array $query_data = [],
        array $connection_config_query_keys = []
    ): object {
        $connection_key = ($connection_key ?? config('connections.default.google'));

        $query_params = array_merge(
            $query_data,
            self::getConnectionQueryParams(
                config('connections.google.' . $connection_key),
                $connection_config_query_keys
            )
        );

        $query_string = !empty($query_params) ? '?' . http_build_query($query_params) : '';

        $request = Http::withToken(ApiToken::run($connection_key))
            ->withHeaders(self::getRequestHeaders())
            ->get($url . $query_string);

        self::logResponse($connection_key, 'get', $url . $query_string, $request);

        if (property_exists($request->object(), 'nextPageToken')) {
            return self::parseApiResponse($request, self::getPaginatedResults(
                $connection_key,
                $url,
                $query_params
            ));
        } else {
            return self::parseApiResponse($request);
        }
    }

    /**
     * Google API POST Request
     *
     * This method is called from other services to perform a POST request and return a structured object.
     *
     * Example Usage:
     *  ```php
     *  $response = ApiClient::post(
     *      $connection_key,
     *      'https://admin.googleapis.com/admin/directory/v1/groups',
     *      [
     *          'name' => 'Hack the Planet Engineers',
     *          'email' => 'elite-engineers@example.com',
     *          'description' => 'This group of engineers hacked the Gibson and have proven they are elite.'
     *      ],
     *      [], // This is optional and can be removed
     *      [], // This is optional and can be removed
     *  );
     *  ```
     *
     * @param ?string $connection_key
     *      The connection key in config/connections.php. Set to null to use default connection key.
     *
     * @param string $url
     *      The full URL of the API endpoint
     *      https://admin.googleapis.com/admin/directory/v1/groups
     *
     * @param array $form_data
     *      Form data array that will be parsed as JSON
     *
     * @param array $query_data
     *      Array of query data to apply to request
     *
     * @param array $connection_config_query_keys
     *      Array of connection configuration keys that should be included with API requests for specific endpoints.
     *      This is mostly used for Google Workspace related API endpoints.
     *      Ex. customer, customer_id, domain, subject_email
     */
    public static function post(
        ?string $connection_key,
        string $url,
        array $form_data = [],
        array $query_data = [],
        array $connection_config_query_keys = []
    ): object {
        $connection_key = ($connection_key ?? config('connections.default.google'));

        $query_params = array_merge(
            $query_data,
            self::getConnectionQueryParams(
                config('connections.google.' . $connection_key),
                $connection_config_query_keys
            )
        );

        $query_string = !empty($query_params) ? '?' . http_build_query($query_params) : '';

        $request = Http::withToken(ApiToken::run($connection_key))
            ->withHeaders(self::getRequestHeaders())
            ->post($url . $query_string, $form_data);

        self::logResponse($connection_key, 'post', $url . $query_string, $request);

        return self::parseApiResponse($request);
    }

    /**
     * Google API PATCH Request
     *
     * This method is called from other services to perform a PATCH request and return a structured object.
     *
     * Example Usage:
     *  ```php
     *  $group_id = 'elite-engineers@example.com';
     *  $response = ApiClient::put(
     *      $connection_key,
     *      'https://admin.googleapis.com/admin/directory/v1/groups/' . $group_id,
     *      [
     *          'name' => 'Elite Engineers',
     *          'email' => 'elite-engineers@example.com',
     *          'description' => 'This group of engineers hacked the Gibson and have proven they are elite.'
     *      ],
     *      [], // This is optional and can be removed
     *      [], // This is optional and can be removed
     *  );
     *  ```
     *
     * @param ?string $connection_key
     *      The connection key in config/connections.php. Set to null to use default connection key.
     *
     * @param string $url
     *      The full URL of the API endpoint
     *      https://admin.googleapis.com/admin/directory/v1/groups/a1b2c3d4e5
     *
     * @param array $form_data
     *      Form data array that will be parsed as JSON
     *
     * @param array $query_data
     *      Array of query data to apply to request
     *
     * @param array $connection_config_query_keys
     *      Array of connection configuration keys that should be included with API requests for specific endpoints.
     *      This is mostly used for Google Workspace related API endpoints.
     *      Ex. customer, customer_id, domain, subject_email
     */
    public static function patch(
        ?string $connection_key,
        string $url,
        array $form_data = [],
        array $query_data = [],
        array $connection_config_query_keys = []
    ): object {
        $connection_key = ($connection_key ?? config('connections.default.google'));

        $query_params = array_merge(
            $query_data,
            self::getConnectionQueryParams(
                config('connections.google.' . $connection_key),
                $connection_config_query_keys
            )
        );

        $query_string = !empty($query_params) ? '?' . http_build_query($query_params) : '';

        $request = Http::withToken(ApiToken::run($connection_key))
            ->withHeaders(self::getRequestHeaders())
            ->patch($url . $query_string, $form_data);

        self::logResponse($connection_key, 'patch', $url . $query_string, $request);

        return self::parseApiResponse($request);
    }

    /**
     * Google API PUT Request
     *
     * This method is called from other services to perform a PUT request and return a structured object.
     *
     * Example Usage:
     *  ```php
     *  $group_id = 'elite-engineers@example.com';
     *  $response = ApiClient::put(
     *      $connection_key,
     *      'https://admin.googleapis.com/admin/directory/v1/groups/' . $group_id,
     *      [
     *          'name' => 'Elite Engineers'
     *      ],
     *      [], // This is optional and can be removed
     *      [], // This is optional and can be removed
     *  );
     *  ```
     *
     * @param ?string $connection_key
     *      The connection key in config/connections.php. Set to null to use default connection key.
     *
     * @param string $url
     *      The full URL of the API endpoint
     *      https://admin.googleapis.com/admin/directory/v1/groups/a1b2c3d4e5
     *
     * @param array $form_data
     *      Form data array that will be parsed as JSON
     *
     * @param array $query_data
     *      Array of query data to apply to request
     *
     * @param array $connection_config_query_keys
     *      Array of connection configuration keys that should be included with API requests for specific endpoints.
     *      This is mostly used for Google Workspace related API endpoints.
     *      Ex. customer, customer_id, domain, subject_email
     */
    public static function put(
        ?string $connection_key,
        string $url,
        array $form_data = [],
        array $query_data = [],
        array $connection_config_query_keys = []
    ): object {
        $connection_key = ($connection_key ?? config('connections.default.google'));

        $query_params = array_merge(
            $query_data,
            self::getConnectionQueryParams(
                config('connections.google.' . $connection_key),
                $connection_config_query_keys
            )
        );

        $query_string = !empty($query_params) ? '?' . http_build_query($query_params) : '';

        $request = Http::withToken(ApiToken::run($connection_key))
            ->withHeaders(self::getRequestHeaders())
            ->put($url . $query_string, $form_data);

        self::logResponse($connection_key, 'put', $url . $query_string, $request);

        return self::parseApiResponse($request);
    }

    /**
     * Google API DELETE Request
     *
     * This method is called from other services to perform a DELETE request and return a structured object.
     *
     * Example Usage:
     *  ```php
     *  $group_id = 'elite-engineers@example.com';
     *  $response = ApiClient::delete(
     *      $connection_key,
     *      'https://admin.googleapis.com/admin/directory/v1/groups/' . $group_id,
     *      [],
     *      [], // This is optional and can be removed
     *      [], // This is optional and can be removed
     *  );
     *  ```
     *
     * @param ?string $connection_key
     *      The connection key in config/connections.php. Set to null to use default connection key.
     *
     * @param string $url
     *      The full URL of the API endpoint
     *      https://admin.googleapis.com/admin/directory/v1/groups/a1b2c3d4e5
     *
     * @param array $form_data
     *      Form data array that will be parsed as JSON.
     *      This is usually empty for delete requests.
     *
     * @param array $query_data
     *      Array of query data to apply to request
     *
     * @param array $connection_config_query_keys
     *      Array of connection configuration keys that should be included with API requests for specific endpoints.
     *      This is mostly used for Google Workspace related API endpoints.
     *      Ex. customer, customer_id, domain, subject_email
     */
    public static function delete(
        ?string $connection_key,
        string $url,
        array $form_data = [],
        array $query_data = [],
        array $connection_config_query_keys = []
    ): object {
        $connection_key = ($connection_key ?? config('connections.default.google'));

        $query_params = array_merge(
            $query_data,
            self::getConnectionQueryParams(
                config('connections.google.' . $connection_key),
                $connection_config_query_keys
            )
        );

        $query_string = !empty($query_params) ? '?' . http_build_query($query_params) : '';

        $request = Http::withToken(ApiToken::run($connection_key))
            ->withHeaders(self::getRequestHeaders())
            ->delete($url . $query_string, $form_data);

        self::logResponse($connection_key, 'delete', $url . $query_string, $request);

        return self::parseApiResponse($request);
    }

    /**
     * Convert API Response Headers to Object
     *
     * This method is called from the parseApiResponse method to prettify the Guzzle Headers that are an array with
     * nested array for each value, and converts the single array values into strings and converts to an object for
     * easier and consistent accessibility with the parseApiResponse format.
     *
     * @param array $header_response
     * [
     *   "ETag" => [
     *     ""nMRgLWac8h8NyH7Uk5VvV4DiNp4uxXg5gNUd9YhyaJE/dky_PFyA8Zq0WLn1WqUCn_A8oes""
     *   ]
     *   "Content-Type" => [
     *     "application/json; charset=UTF-8"
     *   ]
     *   "Vary" => [
     *     "Origin"
     *     "X-Origin"
     *     "Referer"
     *   ]
     *   "Date" => [
     *      "Mon, 24 Jan 2022 15:39:46 GMT"
     *   ]
     *   "Server" => [
     *     "ESF"
     *   ]
     *   "Content-Length" => [
     *     "355675"
     *   ]
     *   "X-XSS-Protection" => [
     *     "0"
     *   ]
     *   "X-Frame-Options" => [
     *     "SAMEORIGIN"
     *   ]
     *   "X-Content-Type-Options" => [
     *     "nosniff"
     *   ]
     *   "Alt-Svc" => [
     *     (truncated)
     *   ]
     * ]
     *
     * @return array
     * [
     *   "ETag" => "nMRgLWac8h8NyH7Uk5VvV4DiNp4uxXg5gNUd9YhyaJE/dky_PFyA8Zq0WLn1WqUCn_A8oes",
     *   "Content-Type" => "application/json; charset=UTF-8",
     *   "Vary" => "Origin X-Origin Referer",
     *   "Date" => "Mon, 24 Jan 2022 15:39:46 GMT",
     *   "Server" => "ESF",
     *   "Content-Length" => "355675",
     *   "X-XSS-Protection" => "0",
     *   "X-Frame-Options" => "SAMEORIGIN",
     *   "X-Content-Type-Options" => "nosniff",
     *   "Alt-Svc" => (truncated)
     * ]
     */
    protected static function convertHeadersToArray(array $header_response): array
    {
        $headers = [];

        foreach ($header_response as $header_key => $header_value) {
            // If array has multiple keys, leave as array
            if (count($header_value) > 1) {
                $headers[$header_key] = $header_value;
            } else {
                $headers[$header_key] = $header_value[0];
            }
        }

        return $headers;
    }

    /**
     * Parse and format single result response
     *
     * @param object $response_object
     *      The API $request->object()
     *
     * @return object
     */
    protected static function getSingleResult(object $response_object): object
    {
        // Unset properties that are not used
        unset($response_object->kind);
        unset($response_object->etag);
        unset($response_object->resultSizeEstimate);

        // Check for response type
        if (collect($response_object)->count() == 1) {
            // This if statement will catch if Google is sending back a response that can be paginated but is not
            // Ex. Google Groups list endpoint but there is only a single group
            return (object) collect($response_object)
                ->flatten(1)
                ->transform(function ($item, $key) {
                    unset($item->kind);
                    unset($item->etag);
                    return $item;
                })->toArray();
        } else {
            return (object) $response_object;
        }
    }

    /**
     * Helper function used to get Google API results that require pagination.
     *
     * @param string $connection_key
     *      The connection key in config/connections.php
     *
     * @param string $url
     *      The full URL of the API endpoint
     *      https://admin.googleapis.com/admin/directory/v1/groups/a1b2c3d4e5
     *
     * @param array $query_data
     *      Array of query data to apply to request
     *
     * @param array $connection_config_query_keys
     *      Array of connection configuration keys that should be included with API requests for specific endpoints.
     *      This is mostly used for Google Workspace related API endpoints.
     *      Ex. customer, customer_id, domain, subject_email
     *
     * @return object
     *      An array of the response objects for each page combined casted as an object.
     */
    protected static function getPaginatedResults(
        string $connection_key,
        string $url,
        array $query_data = [],
        array $connection_config_query_keys = []
    ): object {
        $query_params = array_merge(
            $query_data,
            self::getConnectionQueryParams(
                config('connections.google.' . $connection_key),
                $connection_config_query_keys
            )
        );

        $query_string = !empty($query_params) ? '?' . http_build_query($query_params) : '';

        // Define empty array for adding API results to
        $records = [];

        // Set initial next page token to null that will be overriden in do/while
        $next_page_token = [];

        do {
            $request = Http::withToken(ApiToken::run($connection_key))
                ->withHeaders(self::getRequestHeaders())
                ->get($url, array_merge($query_params, $next_page_token));

            self::logResponse($connection_key, 'get', $url . $query_string, $request);

            $page_records = collect($request->object())
                ->except(['kind', 'etag', 'nextPageToken'])
                ->flatten(1)
                ->transform(function ($item, $key) {
                    unset($item->kind);
                    unset($item->etag);
                    return $item;
                })->toArray();

            $records = array_merge($records, $page_records);

            // Get token for next page of results
            if (property_exists($request->object(), 'nextPageToken')) {
                $next_page_token = ['pageToken' => $request->object()->nextPageToken];
            } else {
                $next_page_token = [];
            }
        } while ($next_page_token != []);

        return (object) $records;
    }

    /**
     * Parse the API response and return custom formatted response for consistency
     *
     * @see https://laravel.com/docs/10.x/http-client#making-requests
     *
     * @param object $response Response object from API results
     *
     * @param object $paginated_object Results array from paginated responses
     *
     * @return object Custom response returned for consistency
     * ```php
     * {
     *   +"headers" => [
     *     "ETag" => (truncated),
     *     "Content-Type" => "application/json; charset=UTF-8",
     *     "Vary" => "Origin X-Origin Referer",
     *     "Date" => "Mon, 24 Jan 2022 17:25:15 GMT",
     *     "Server" => "ESF",
     *     "Content-Length" => "1259",
     *     "X-XSS-Protection" => "0",
     *     "X-Frame-Options" => "SAMEORIGIN",
     *     "X-Content-Type-Options" => "nosniff",
     *     "Alt-Svc" => (truncated)
     *   ],
     *   +"json": (truncated)
     *   +"object": {
     *     +"kind": "admin#directory#user"
     *     +"id": "114522752583947996869"
     *     +"etag": (truncated)
     *     +"primaryEmail": "klibby@example.com"
     *     +"name": {
     *       +"givenName": "Kate"
     *       +"familyName": "Libby"
     *       +"fullName": "Kate Libby"
     *     }
     *     +"isAdmin": true
     *     +"isDelegatedAdmin": false
     *     +"lastLoginTime": "2022-01-21T17:44:13.000Z"
     *     +"creationTime": "2021-12-08T13:15:43.000Z"
     *     +"agreedToTerms": true
     *     +"suspended": false
     *     +"archived": false
     *     +"changePasswordAtNextLogin": false
     *     +"ipWhitelisted": false
     *     +"emails": array:3 [
     *       0 => {
     *         +"address": "klibby@example.com"
     *         +"type": "work"
     *       }
     *       1 => {
     *         +"address": "klibby@example-test.com"
     *         +"primary": true
     *       }
     *       2 => {
     *         +"address": "klibby@example.com.test-google-a.com"
     *       }
     *     ]
     *     +"phones": array:1 [
     *       0 => {
     *         +"value": "5555555555"
     *         +"type": "work"
     *       }
     *     ]
     *     +"languages": array:1 [
     *       0 => {
     *         +"languageCode": "en"
     *         +"preference": "preferred"
     *       }
     *     ]
     *     +"nonEditableAliases": array:1 [
     *       0 => "klibby@example.com.test-google-a.com"
     *     ]
     *     +"customerId": "C000nnnnn"
     *     +"orgUnitPath": "/"
     *     +"isMailboxSetup": true
     *     +"isEnrolledIn2Sv": false
     *     +"isEnforcedIn2Sv": false
     *     +"includeInGlobalAddressList": true
     *   }
     *   +"status": {#1269
     *     +"code": 200
     *     +"ok": true
     *     +"successful": true
     *     +"failed": false
     *     +"serverError": false
     *     +"clientError": false
     *   }
     */
    protected static function parseApiResponse(object $response, object $paginated_object = null): object
    {
        return (object) [
            'headers' => self::convertHeadersToArray($response->headers()),
            'object' => (object) ($paginated_object ?? self::getSingleResult($response->object())),
            'status' => (object) [
                'code' => $response->status(),
                'ok' => $response->ok(),
                'successful' => $response->successful(),
                'failed' => $response->failed(),
                'serverError' => $response->serverError(),
                'clientError' => $response->clientError(),
            ],
        ];
    }

    /**
     * Create a log entry for an API call
     *
     * This method is called from other methods and create log entry and throw exception
     *
     * @param string $connection_key
     *      The connection key in config/connections.php
     *
     * @param string $method
     *      The lowercase name of the method that calls this function (ex. `get`)
     *
     * @param string $url
     *      The URL of the API call including the concatenated base URL and URI
     *
     * @param object $response
     *      The raw unformatted HTTP client response
     */
    protected static function logResponse(string $connection_key, string $method, string $url, object $response): void
    {
        $log_type = [
            200 => ['event_type' => 'google-api.response.info.ok', 'level' => 'info'],
            201 => ['event_type' => 'google-api.response.info.created', 'level' => 'info'],
            202 => ['event_type' => 'google-api.response.info.accepted', 'level' => 'info'],
            204 => ['event_type' => 'google-api.response.info.deleted', 'level' => 'info'],
            400 => ['event_type' => 'google-api.response.warning.bad-request', 'level' => 'warning'],
            401 => ['event_type' => 'google-api.response.error.unauthorized', 'level' => 'error'],
            403 => ['event_type' => 'google-api.response.error.forbidden', 'level' => 'error'],
            404 => ['event_type' => 'google-api.response.warning.not-found', 'level' => 'warning'],
            405 => ['event_type' => 'google-api.response.error.method-not-allowed', 'level' => 'error'],
            412 => ['event_type' => 'google-api.response.error.precondition-failed', 'level' => 'error'],
            422 => ['event_type' => 'google-api.response.error.unprocessable', 'level' => 'error'],
            429 => ['event_type' => 'google-api.response.critical.rate-limit', 'level' => 'critical'],
            500 => ['event_type' => 'google-api.response.critical.server-error', 'level' => 'critical'],
            501 => ['event_type' => 'google-api.response.error.not-implemented', 'level' => 'error'],
            503 => ['event_type' => 'google-api.response.critical.server-unavailable', 'level' => 'critical'],
        ];

        $log_context = [
            'api_endpoint' => $url,
            'api_method' => Str::upper($method),
            'class' => get_class(),
            'connection_key' => $connection_key,
            'event_type' => $log_type[$response->status->code]['event_type'],
            'status_code' => $response->status->code,
        ];

        if (property_exists($response->object(), 'error')) {
            $log_context['reason'] = $response->object()->error;
        } elseif (!$response->successful() && isset($response->object()->message)) {
            $log_context['reason'] = $response->json;
        } elseif (!$response->successful()) {
            $log_context['reason'] = null;
        }

        ksort($log_context);

        Log::write(
            $log_type[$response->status->code]['level'],
            Str::upper($method) . ' ' . $response->status->code . ' ' . $url,
            $log_context
        );
    }

    /**
     * Encoding schema utilized by Google OAuth2 Servers
     *
     * @see https://stackoverflow.com/a/65893524
     *
     * @param string $input
     *      The input string to encode
     *
     * @return string
     */
    protected static function base64UrlEncode(string $input): string
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }

    /**
     * Generate an array of request headers for the API request
     */
    private static function getRequestHeaders(): array
    {
        $composer_array = json_decode((string) file_get_contents(base_path('composer.json')), true);
        $package_name = Str::title($composer_array['name']);

        return [
            'User-Agent' => $package_name . ' ' . 'Laravel/' . app()->version() . ' ' . 'PHP/' . phpversion()
        ];
    }

    /**
     * Add connection config array values to the request data included with all API requests
     *
     * @param array $connection_config
     *      The array from `config('connections.google.' . $connection_key)`
     *
     * @param array $connection_config_keys
     *      The array keys from the connection's config array to add to request data
     *      Ex. customer_id, domain, subject_email
     */
    private static function getConnectionQueryParams(array $connection_config, array $connection_config_keys): array
    {
        $connection_query_params = [];
        foreach ($connection_config_keys as $key) {
            if (array_key_exists($key, $connection_config)) {
                $connection_query_params[$key] = $connection_config[$key];
            } else {
                throw new ConfigurationException(
                    'The connection configuration key (' . $key . ') does not exist for this connection.'
                );
            }
        }

        return $connection_query_params;
    }
}
