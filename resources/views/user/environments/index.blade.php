@extends('user._layouts.core')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">

            <!-- Page Header -->
            <div class="d-flex mb-4">
                <div class="flex-grow-1">
                    <h3>Terraform Environments</h3>
                </div>
                <div class="">
                    @if($request->user()->flag_git_user_provisioned == true)
                        <a class="btn btn-primary mr-2" data-toggle="modal" data-target="#iam-credentials" href="#">View GitOps Credentials</a>
                    @else
                        <button class="btn btn-secondary mr-2" disabled data-toggle="tooltip" data-placement="top" title="Your GitOps credentials have not been generated. Please contact support for assistance.">View GitOps Credentials</button>
                    @endif
                    @if(in_array('user.environments.create', $request->user()->permissions))
                        <a class="btn btn-primary" href="{{ route('user.environments.create') }}">Create Terraform Environment</a>
                    @else
                        <button class="btn btn-secondary" disabled data-toggle="tooltip" data-placement="top" title="You do not have permission to create a Terraform environment.">Create Terraform Environment</button>
                    @endif
                </div>
            </div>
            <!-- END Page Header -->

            @if($request->user()->flag_git_user_provisioned == false)
                <div class="alert alert-danger">
                    Your GitOps credentials have not been generated. This is likely due to changes in recent releases that were not applied to your existing user account. Please contact the system administrator for assistance.
                </div>
            @endif

            <div class="alert alert-info">
                A starter Terraform environment is created for each Cloud Account. You can create additional Terraform environments to isolate your infrastructure resources for each use case.
            </div>

            <div class="card">
                @foreach($cloud_account_users as $cloud_account_user)
                    @if($cloud_account_user->cloudAccount->cloudAccountEnvironments->count() > 0)
                        <table class="table mb-0">
                            <tbody>
                                @foreach($cloud_account_user->cloudAccount->cloudAccountEnvironments as $environment)
                                    <tr>
                                        <td style="width: 75%;">
                                            <div>
                                                <span class="badge badge-secondary mr-1">{{ $environment->short_id }}</span>
                                                <a href="{{ route('user.cloud.accounts.show', ['id' => $environment->cloud_account_id]) }}"><strong>{{ $environment->name }}</strong></a>
                                            </div>
                                            <div class="text-muted small {{ $environment->description != null ? 'mb-2' : '' }}">
                                                {{ $cloud_account_user->cloudAccount->slug }}-{{ $cloud_account_user->cloudAccount->short_id }}
                                            </div>
                                            {{ $environment->description ? Str::limit($environment->description, 254, '...') : '' }}
                                        </td>
                                        <td class="d-flex flex-row-reverse">
                                            <div class="btn-toolbar">
                                                <div class="btn-group">
                                                    <a class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="View Details" data-original-title="View Details" href="{{ route('user.cloud.accounts.show', ['id' => $environment->cloud_account_id]) }}">
                                                        <i class="ri-settings-3-line ri-lg"></i>
                                                    </a>
                                                    <a class="btn btn-outline-primary" target="_blank" data-toggle="tooltip" data-placement="top" title="View Terraform Configuration" data-original-title="View Terraform Configuration" href="{{ $environment->git_meta_data['web_url'] }}">
                                                        <i class="ri-git-repository-fill ri-lg"></i>
                                                    </a>
                                                    @if($cloud_account_user->authTenant->git_provider_type == 'gitlab')
                                                        <a class="btn btn-outline-primary" target="_blank" data-toggle="tooltip" data-placement="top" title="View CI Pipelines" data-original-title="View CI Pipelines" href="{{ $environment->git_meta_data['web_url'] }}/-/pipelines">
                                                            <i class="ri-rocket-fill ri-lg"></i>
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                @endforeach
                            </tbody>
                        </table>
                    @endif
                @endforeach
            </div>

        </div>
    </div>

    {{-- If authenticated user has a Git user account provisioned --}}
    @if($request->user()->flag_git_user_provisioned == true)

        {{-- Include the modal and pass variable through with user credentials --}}
        @include('user.environments._modals.iam-credentials')

    @endif

@endsection

@section('javascript')

@endsection
