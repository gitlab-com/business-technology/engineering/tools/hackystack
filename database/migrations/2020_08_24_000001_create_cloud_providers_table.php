<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCloudProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cloud_providers', function (Blueprint $table) {
            $table->uuid('id')->nullable()->index();
            $table->string('short_id', 8)->nullable()->index();
            $table->uuid('auth_tenant_id')->nullable()->index();
            $table->text('api_credentials')->nullable()->comment('encrypted');
            $table->json('api_meta_data')->nullable();
            $table->json('git_meta_data')->nullable();
            $table->string('type')->index();
            $table->string('name');
            $table->string('slug')->nullable()->index();
            $table->string('parent_org_unit')->nullable();
            $table->json('default_roles')->nullable();
            $table->boolean('flag_provisioned')->default(false)->nullable();
            $table->timestamp('provisioned_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->string('state', 55)->nullable();
            $table->unique(['auth_tenant_id', 'slug'], 'cloud_providers_tenant_slug_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cloud_providers');
    }
}
