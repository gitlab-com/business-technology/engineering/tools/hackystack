<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthUserResetPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-user:reset-password
                            {short_id? : The short ID of the user.}
                            {--P|password= : The password for the user. If not set, a randomly generated password will be created}
                            {--must-change-password : Whether the user will be prompted to change their password when they sign in}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset the Password for an Authentication User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short_id to lookup the record.');
            $this->comment('You can lookup by short ID using `auth-user:delete a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_user = Models\Auth\AuthUser::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($auth_user == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Password
        $password = $this->option('password');
        if($password == null) {
            $password = Str::random(12);
        }

        // Change Password Flag
        if($this->option('must-change-password') == true) {
            $flag_must_change_password = 1;
        } else {
            $flag_must_change_password = 0;
        }

        // Initialize service
        $authUserService = new Services\V1\Auth\AuthUserService();

        //
        // Update User Record
        // --------------------------------------------------------------------
        //

        $authUserService->update($auth_user->id, [
            'password' => $password,
            'flag_must_change_password' => $flag_must_change_password,
            'notify_send_password_change_email' => 0,
        ]);

        // Refresh the Eloquent model variable with the latest values
        $auth_user = $auth_user->fresh();

        //
        // Show User Record
        // --------------------------------------------------------------------
        //

        // Auth User Values
        $this->comment('');
        $this->comment('Auth User');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_user->id
                ],
                [
                    'short_id',
                    $auth_user->short_id
                ],
                [
                    'auth_tenant',
                    '['.$auth_user->authTenant->short_id.'] '.$auth_user->authTenant->slug,
                ],
                [
                    'auth_provider',
                    '['.$auth_user->authProvider->short_id.'] '.$auth_user->authProvider->slug,
                ],
                [
                    'full_name',
                    $auth_user->full_name
                ],
                [
                    'email',
                    $auth_user->email
                ],
                [
                    'flag_must_change_password',
                    $auth_user->flag_must_change_password
                ],
                [
                    'flag_account_verified',
                    $auth_user->flag_account_verified
                ],
            ]
        );

        $this->info('');
        $this->info('Password: '.$password);
        $this->error('The password will not be shown again. Please copy it to a secure location.');

        $this->comment('');
        // Show result in console
        $this->comment('Password changed successfully.');
        $this->comment('');

    }
}
