<?php

namespace App\Services\V1\Vendor\Aws;

use App\Services\V1\Vendor\Aws\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class IamUserRoleService extends BaseService
{

    protected $aws_api_client;

    public function __construct($cloud_provider_id, $aws_child_account_id = null)
    {
        // Call BaseService methods to establish API connection
        $this->setCloudProvider($cloud_provider_id);

        // Decrypt the provider credentials to be able to use the array keys
        $credentials = json_decode(decrypt($this->cloud_provider->api_credentials), true);

        // If AWS child account ID is set, we will use AWS Security Token
        // Service (STS) to get temporary credentials that use the assumeRole
        // functionality to get Administrator access to the child AWS account.
        if($aws_child_account_id != null) {

            // Initialize STS Client using cloud provider credentials
            $aws_sts_client = new \Aws\Sts\StsClient([
                'version' => 'latest',
                'region' => 'us-east-1',
                'credentials' => [
                    'key' => $credentials['aws_access_key_id'],
                    'secret' => $credentials['aws_access_key_secret'],
                ],
            ]);

            // Use STS to assume role and get credentials for child account
            $child_account_credentials = $aws_sts_client->assumeRole([
                'DurationSeconds' => '900',
                'ExternalId' => $aws_child_account_id,
                'RoleArn' => 'arn:aws:iam::'.$aws_child_account_id.':role/OrganizationAccountAccessRole',
                'RoleSessionName' => 'HackyStackIamUserRoleService'.now()->format('YmdHiSu')
            ]);

            // Initialize the API client
            $this->aws_api_client = new \Aws\Iam\IamClient([
                'version' => 'latest',
                'region'  => 'us-east-1',
                'credentials' => [
                    'key'    => $child_account_credentials['Credentials']['AccessKeyId'],
                    'secret' => $child_account_credentials['Credentials']['SecretAccessKey'],
                    'token' => $child_account_credentials['Credentials']['SessionToken']
                ],
            ]);

        }

        // If AWS child account ID is not set, the actions will be performed
        // in the organization root account.
        else {

            // Initialize the API client
            $this->aws_api_client = new \Aws\Iam\IamClient([
                'version' => 'latest',
                'region'  => 'us-east-1',
                'credentials' => [
                    'key'    => $credentials['aws_access_key_id'],
                    'secret' => $credentials['aws_access_key_secret'],
                ],
            ]);

        }

    }

    public function all($request_data = [])
    {

        //
    }

    /**
     *   Attach an IAM user to an IAM Policy (role)
     *   https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#attachuserpolicy
     *
     *   @param  array  $request_data
     *      username|string             Alphadash username for the AWS IAM User
     *      policy|string               Policy name (ex. AdministratorAccess)
     *
     *   @return array
     */
    public function create($request_data = [])
    {
        // Attach an AWS-managed job function permission policy
        $aws_iam_user_policy = $this->aws_api_client->attachUserPolicy([
            'PolicyArn' => 'arn:aws:iam::aws:policy/'.$request_data['policy'],
            'UserName' => $request_data['username'],
        ]);

        return $aws_iam_user_policy;

    }

    /**
     *   Detach an IAM user from an IAM Policy (role)
     *   https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#detachuserpolicy
     *
     *   @param  array  $request_data
     *      username|string             Alphadash username for the AWS IAM User
     *      policy|string               Policy name (ex. AdministratorAccess)
     *
     *   @return array
     */
    public function delete($id)
    {
        // Detach an AWS-managed job function permission policy
        $aws_iam_user_policy = $this->aws_api_client->detachUserPolicy([
            'PolicyArn' => 'arn:aws:iam::aws:policy/'.$request_data['policy'],
            'UserName' => $request_data['username'],
        ]);

        return $aws_iam_user_policy;
    }

}
